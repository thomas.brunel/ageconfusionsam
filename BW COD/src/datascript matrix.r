library(stockassessment)
agesampledata<-read.table("../294.csv", sep=";", head=TRUE)
agesampledata<-agesampledata[agesampledata$expertise==1,]

agesampledata<-cbind(agesampledata[,c("age","sample")],data=1:c(dim(agesampledata)[1]))

ACM<-structure(c(1, 0.02, 0, 0, 0, 0, 0, 0, 0, 0, 0.91, 0.07, 0, 0, 
0, 0, 0, 0, 0, 0.07, 0.82, 0.09, 0, 0, 0, 0, 0, 0, 0, 0.11, 0.79, 
0.09, 0, 0, 0, 0, 0, 0, 0, 0.12, 0.8, 0.08, 0, 0, 0, 0, 0, 0, 
0, 0.11, 0.82, 0.06, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.87, 0.04, 0, 
0, 0, 0, 0, 0, 0, 0.07, 0.91, 0.02, 0, 0, 0, 0, 0, 0, 0, 0.05, 
0.98), .Dim = c(9L, 9L))

#################################################
#################################################
##      0    1    2    3    4    5    6    7    8
## 0  1.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00
## 1  0.02 0.91 0.07 0.00 0.00 0.00 0.00 0.00 0.00
## 2  0.00 0.07 0.82 0.11 0.00 0.00 0.00 0.00 0.00
## 3  0.00 0.00 0.09 0.79 0.12 0.00 0.00 0.00 0.00
## 4  0.00 0.00 0.00 0.09 0.80 0.11 0.00 0.00 0.00
## 5  0.00 0.00 0.00 0.00 0.08 0.82 0.10 0.00 0.00
## 6  0.00 0.00 0.00 0.00 0.00 0.06 0.87 0.07 0.00
## 7  0.00 0.00 0.00 0.00 0.00 0.00 0.04 0.91 0.05
## 8  0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.02 0.98

ACM.cn<-ACM[2:8,]
ACM.cn[,2]<-ACM.cn[,2]+ACM.cn[,1]
ACM.cn[,8]<-ACM.cn[,8]+ACM.cn[,9]
ACM.cn<-ACM.cn[,2:8]

ACM.s1<-ACM[1:5,]
ACM.s1[,5]<-rowSums(ACM.s1[,5:9])
ACM.s1<-ACM.s1[,1:5]

ACM.s2<-ACM[2:5,]
ACM.s2[,2]<-ACM.s2[,2]+ACM.s2[,1]
ACM.s2[,5]<-rowSums(ACM.s2[,5:9])
ACM.s2<-ACM.s2[,2:5]


oldwd<-setwd("../data")

  cn<-read.ices("cn.dat")
  W<-matrix(1, nrow=nrow(cn), ncol=ncol(cn)); W[nrow(cn),]<-0.1; attr(cn,"weight")<-W
  attr(cn,"ageConfusion")<-ACM.cn

  cw<-read.ices("cw.dat")
  dw<-read.ices("dw.dat")
  lw<-read.ices("lw.dat")
  mo<-read.ices("mo.dat")
  nm<-read.ices("nm.dat")
  pf<-read.ices("pf.dat")
  pm<-read.ices("pm.dat")
  sw<-read.ices("sw.dat")
  lf<-read.ices("lf.dat")
  surveys<-read.ices("survey.dat")
  attr(surveys[[1]],"ageConfusion")<-ACM.s1
  attr(surveys[[2]],"ageConfusion")<-ACM.s2
setwd(oldwd)

dat_mat_read<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf, agesampledata=agesampledata)

dat_mat_noread<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf, agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))



save(dat_mat_read,dat_mat_noread, file="../run/data matrix.RData")









cn<-read.ices("../data/cn.dat")

library(matlib)
cn2 <- inv(t(ACM.cn)) %*% t(cn)
cn2<-t(cn2)

colnames(cn2)  <- paste("age", 1:7)
colnames(cn)  <- paste("age", 1:7)

cndf  <- tidyr::gather(data.frame(year=1985:2020,cn), "age" , "catch" ,-year)
cndf2 <- tidyr::gather(data.frame(year=1985:2020,cn2), "age" , "catch" ,-year)
cndf$origin <- "original"
cndf2$origin <- "back transformed"

cdf <- rbind(cndf,cndf2)

library(ggplot2)
ggplot(cdf , aes(year,catch,colour = origin)) + geom_line() + facet_wrap(~factor(age,levels = ( paste("age.", 0:12,sep=""))))

