library(stockassessment)
setwd("../run")


# base model : no age reading used
load("data.RData")
conf<-loadConf(dat,"../conf/model.cfg", patch=TRUE)
conf$keyAgeSampleData[]<-NA
par<-defpar(dat,conf)
fit_base<-sam.fit(dat,conf, par, newtonsteps=FALSE)
if(fit_base$opt$convergence!=0) stop("Model did not converge.")



# model with exchange data, estimating the error matrix
conf<-loadConf(dat,"../conf/model.cfg", patch=TRUE)
par<-defpar(dat,conf)
fit_read<-sam.fit(dat,conf, par, newtonsteps=FALSE)
if(fit_read$opt$convergence!=0) stop("Model did not converge.")


# model with the exchange data and the error matrix provided
load("data matrix.RData")
conf<-loadConf(dat_mat_read,"../conf/model.cfg", patch=TRUE)
par<-defpar(dat_mat_read,conf)
fit_mat_read<-sam.fit(dat_mat_read,conf, par, newtonsteps=FALSE)
if(fit_mat_read$opt$convergence!=0) stop("Model did not converge.")



# model without the exchange data only the error matrix provided
conf$keyAgeSampleData[]<-NA
par<-defpar(dat_mat_read,conf)
fit_mat<-sam.fit(dat_mat_read,conf, par, newtonsteps=FALSE)
if(fit_mat$opt$convergence!=0) stop("Model did not converge.")



# same as above, but with ACM estimated externally
conf$keyAgeSampleData[]<-NA

load("../../../ageConfusionExample/ACMall.RData")
dat_mat_read$ageConfusion[[1]] <- ACM.cn
dat_mat_read$ageConfusion[[2]] <- diag(nrow=5)
dat_mat_read$ageConfusion[[3]] <- diag(nrow=4)

par<-defpar(dat_mat_read,conf)
fit_mat<-sam.fit(dat_mat_read,conf, par, newtonsteps=FALSE)
if(fit_mat$opt$convergence!=0) stop("Model did not converge.")





# plot the ACM and compute back transform catch matrix
cn<-read.ices("../data/cn.dat")
ACM<-structure(c(1, 0.02, 0, 0, 0, 0, 0, 0, 0, 0, 0.91, 0.07, 0, 0, 
0, 0, 0, 0, 0, 0.07, 0.82, 0.09, 0, 0, 0, 0, 0, 0, 0, 0.11, 0.79, 
0.09, 0, 0, 0, 0, 0, 0, 0, 0.12, 0.8, 0.08, 0, 0, 0, 0, 0, 0, 
0, 0.11, 0.82, 0.06, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.87, 0.04, 0, 
0, 0, 0, 0, 0, 0, 0.07, 0.91, 0.02, 0, 0, 0, 0, 0, 0, 0, 0.05, 
0.98), .Dim = c(9L, 9L))

#################################################
#################################################
##      0    1    2    3    4    5    6    7    8
## 0  1.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00
## 1  0.02 0.91 0.07 0.00 0.00 0.00 0.00 0.00 0.00
## 2  0.00 0.07 0.82 0.11 0.00 0.00 0.00 0.00 0.00
## 3  0.00 0.00 0.09 0.79 0.12 0.00 0.00 0.00 0.00
## 4  0.00 0.00 0.00 0.09 0.80 0.11 0.00 0.00 0.00
## 5  0.00 0.00 0.00 0.00 0.08 0.82 0.10 0.00 0.00
## 6  0.00 0.00 0.00 0.00 0.00 0.06 0.87 0.07 0.00
## 7  0.00 0.00 0.00 0.00 0.00 0.00 0.04 0.91 0.05
## 8  0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.02 0.98

ACM.cn<-ACM[2:8,]
ACM.cn[,2]<-ACM.cn[,2]+ACM.cn[,1]
ACM.cn[,8]<-ACM.cn[,8]+ACM.cn[,9]
ACM.cn<-ACM.cn[,2:8]
apply(ACM.cn,1,sum)

library(matlib)
cn2 <- inv(t(ACM.cn)) %*% t(cn)
cn2<-t(cn2)

colnames(cn2)  <- paste("age", 1:7)
colnames(cn)  <- paste("age", 1:7)

cndf  <- tidyr::gather(data.frame(year=1985:2020,cn), "age" , "catch" ,-year)
cndf2 <- tidyr::gather(data.frame(year=1985:2020,cn2), "age" , "catch" ,-year)
cndf$origin <- "original"
cndf2$origin <- "back transformed"

cdf <- rbind(cndf,cndf2)

library(ggplot2)
ggsave("../../plots/backtransformedCatch_WBCod_empiricalACM.png",
ggplot(cdf , aes(year,catch,colour = origin)) + geom_line() + facet_wrap(~factor(age,levels = ( paste("age.", 0:12,sep=""))))
)
colnames(cn)  <- as.character(0:12)


library(ggcorrplot)
matpl<-ACM.cn
row.names(matpl) <- paste("true age",1:7)
colnames(matpl) <- paste("age read",1:7)
ggsave("../../plots/empirical ACM WB Cod.png",
ggcorrplot(t(matpl),lab=T,title="ACM empirical WB cod",legend.title = "Prop")
)









#compate models 
fit_base
fit_read
fit_mat_read   # same as above
fit_mat


plot(partable(fit)[,"exp(par)"],partable(fit_mat_read)[,"exp(par)"])
plot(ssbtable(fit)[,"Estimate"],ssbtable(fit_mat_read)[,"Estimate"])
plot(ssbtable(fit)[,"Estimate"],ssbtable(fit_mat_read2)[,"Estimate"])


par(mfrow=c(2,2),mar=c(4,5,1,1))
ssbplot(fit_base    ,col="blue",cicol=rgb(0,0,1,0.1),main="SSB")
ssbplot(fit_read    ,add=T,col="red",cicol=rgb(1,0,0,0.1))
ssbplot(fit_mat     ,add=T,col="green",cicol=rgb(0,1,0,0.1))

legend(legend = c("base","estimated","provided"), col = c("blue", "red","green") ,lwd=2 , lty=c(1),x="top" ,bg="white")

fbarplot(fit_base     ,partial=F,col="blue",cicol=rgb(0,0,1,0.1),main="Fbar")
fbarplot(fit_read     ,add=T,partial=F,col="red",cicol=rgb(1,0,0,0.1))
fbarplot(fit_mat      ,add=T,partial=F,col="green",cicol=rgb(0,1,0,0.1))


recplot ( fit_base    ,col="blue",cicol=rgb(0,0,1,0.1),main = "Recruitment" , las=0, drop=1)
recplot ( fit_read    ,add=T,col="red",cicol=rgb(1,0,0,0.1),las=0, drop=1)
recplot ( fit_mat     ,add=T,col="green",cicol=rgb(1,1,0,0.1),las=0, drop=1)


catchplot( fit_base   ,col="blue",cicol=rgb(1,0,1,0.1) , main = "Catch")
catchplot( fit_read   ,add=T,col="red",cicol=rgb(1,0,0,0.1))
catchplot( fit_mat     ,add=T,col="red",cicol=rgb(1,0,0,0.1))











# what are the new parameters  ? 

# logAgeASD = mean age reading for a sample (log +0.5)
agedt <-  dat$agesampledata
lnmean<-aggregate(age ~sample ,data=agedt,function(x) mean((log(x+0.5))))
pars <- (par$logAgeASD)
lnmean <- cbind(lnmean,pars)
plot(lnmean$age,lnmean$pars)


fitted <- fit$sdrep$par.fixed[names(fit$sdrep$par.fixed) == "logAgeASD"]
points(lnmean$age ,fitted, col="red")

lnmean <- cbind(lnmean,fitted)
