
cat("!! please check : some values are hardcoded and need to be updated each year \n")

cn<-read.ices("data/cn.dat")
cw<-read.ices("data/cw.dat")
dw<-read.ices("data/dw.dat")
lf<-read.ices("data/lf.dat")
lw<-read.ices("data/lw.dat")
mo<-read.ices("data/mo.dat")
nm<-read.ices("data/nm.dat")
pf<-read.ices("data/pf.dat")
pm<-read.ices("data/pm.dat")
sw<-read.ices("data/sw.dat")
surveys<-read.ices("data/survey.dat")

recap<-read.table("data/tag_steel.dat", header=TRUE)
recap<-recap[recap$Type==1 & recap$RecaptureY<=2006,]
recap<-recap[recap[,1]>=min(as.numeric(rownames(sw))), ]

recap2<-read.table("data/tag_RFID.dat", header=TRUE)
recap2$r<-round(recap2$r)
recap2<-recap2[recap2$Nscan>0,]
age <- recap2$ReleaseY - recap2$Yearclass
# subset of the data according to changes made in IBP 2019
recap2 <- recap2[age>4,]
recap2 <- recap2[(recap2$RecaptureY-recap2$ReleaseY) %in% c(1,2), ]
recap2 <- recap2[recap2$ReleaseY>=2013, ]

recap<-rbind(recap,recap2)

age<- recap$ReleaseY - recap$Yearclass
recap  <- recap[age>1 & age<12,]     # possibly test id <13 ok???

#### Remove the tags recapture in the release year
recap <- recap[recap$ReleaseY!=recap$RecaptureY,]

recap <- recap[recap$RecaptureY!=2021,]     # remove scans of catch in the current Y



W<-matrix(NA,nrow=nrow(cn), ncol=ncol(cn))
W[as.numeric(rownames(cn))<2000]<-10
attr(cn,"weight")<-W

#
## from the otolith workshop 
#ACM.cn<-  read.csv("./data/2011 ageing errors.csv")
#ACM.cn <- structure(as.vector(t(ACM.cn)[-1,]),.Dim = c(13L, 13L))
#
# or modelled
load("data/ACM Mac.RData")
ACM.cn <- structure(as.vector((ACM.cn)),.Dim = c(13L, 13L))
apply(ACM.cn,1,sum)
attr(cn,"ageConfusion")<-ACM.cn


