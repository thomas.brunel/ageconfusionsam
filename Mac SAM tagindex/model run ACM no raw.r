rm(list=ls())

###  R4.1.1 have the correct version of the package

# devtools::install_github("fishfollower/SAM/stockassessment")
library(stockassessment)


# read in data -----------------------------------------------------------------
source("datascriptACMfitted_noraw.R")


#check ACM
cn
# try with ID matrix
attr(cn,"ageConfusion")   <- diag(nrow = 13)

dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))

#### model configuration

conf<-defcon(dat)
conf$keyLogFsta[1,] <- c(0,1,2,3,4,5,6,7,7,7,7,7,7)
conf$keyVarObs[4,]      <- c(-1,-1,-1, 3,4,4,4,4,4,4,4,4,-1)
conf$fbarRange <- c(4,8)
conf$corFlag <- 0
conf$fixVarToWeight<-1
conf$obsCorStruct[] <- c("ID", "ID", "ID", "AR", "ID")
#conf$keyCorObs[4,7:11]<-0
conf$keyCorObs[4,]<-c(-1 ,-1, -1 , 0  ,0 , 0,  0,  0,  0,   0 ,  0 , -1)

conf$keyVarF[1,]    <- c(0,1,rep(2,6),rep(-1,5))

conf$keyVarObs[1,]   <- c( 0,1,rep(2,11) )
conf$keyVarObs[2,1]  <- 3
conf$keyVarObs[3,1]  <- 4
conf$keyVarObs[4,]   <- c(-1, -1, -1,  5, 6,  6,  6,  6, 6,  6,  6,   6,  -1 )
conf$keyVarObs[5,]      <- c(-1,-1, 7, 7, 7, 7, 7, 7, 7,  7,  7,  7, -1)
confsave <-conf

#conf$keyAgeSampleData[]<-NA                       # no raw age reading used
#conf$keyXtraSd <- matrix(numeric(0),0,0)          # no sure what this is but needs to be  <0 x 0 matrix>
#conf$offsetAgeSampleData[] <- NA                  # only one offset defined, for the catches

par<-defpar(dat,conf)


fit.new3 <-sam.fit(dat,conf,par)
partable(fit.new3)
save(fit.new3, file="model fit ACM.RData")


load("model fit check.RData")
fit.new2
fit.new3











# model fitting : no raw data, ACM fitted externally ----------------------------------------------------------------
attr(cn,"ageConfusion")   <- ACM.cn
dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))


apply(dat$ageConfusion[[1]],1,sum)


#### model configuration
conf<-defcon(dat)
conf$keyLogFsta[1,] <- c(0,1,2,3,4,5,6,7,7,7,7,7,7)
conf$keyVarObs[4,]      <- c(-1,-1,-1, 3,4,4,4,4,4,4,4,4,-1)
conf$fbarRange <- c(4,8)
conf$corFlag <- 0
conf$fixVarToWeight<-1
conf$obsCorStruct[] <- c("ID", "ID", "ID", "AR", "ID")
#conf$keyCorObs[4,7:11]<-0
conf$keyCorObs[4,]<-c(-1 ,-1, -1 , 0  ,0 , 0,  0,  0,  0,   0 ,  0 , -1)

conf$keyVarF[1,]    <- c(0,1,rep(2,6),rep(-1,5))

conf$keyVarObs[1,]   <- c( 0,1,rep(2,11) )
conf$keyVarObs[2,1]  <- 3
conf$keyVarObs[3,1]  <- 4
conf$keyVarObs[4,]   <- c(-1, -1, -1,  5, 6,  6,  6,  6, 6,  6,  6,   6,  -1 )
conf$keyVarObs[5,]      <- c(-1,-1, 7, 7, 7, 7, 7, 7, 7,  7,  7,  7, -1)
confsave <-conf

# ACM specific parameters
conf$keyAgeSampleData[]<-NA                       # no raw age reading used
conf$keyXtraSd <- matrix(numeric(0),0,0)          # no sure what this is but needs to be  <0 x 0 matrix>
conf$offsetAgeSampleData[] <- NA                  # only one offset defined, for the catches
par<-defpar(dat,conf)

fit.new4 <-sam.fit(dat,conf,par)




# model fitting : no raw data, weighted mean between externally fitted ACM  and ID   p=3%----------------------------------------------------------------
wt.acm <- 0.03
ACMmean <- wt.acm * ACM.cn + (1-wt.acm)* diag(nrow=13)
apply(ACMmean,1,sum)


attr(cn,"ageConfusion")   <- ACMmean
dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))

#### model configuration
conf<-defcon(dat)
conf$keyLogFsta[1,] <- c(0,1,2,3,4,5,6,7,7,7,7,7,7)
conf$keyVarObs[4,]      <- c(-1,-1,-1, 3,4,4,4,4,4,4,4,4,-1)
conf$fbarRange <- c(4,8)
conf$corFlag <- 0
conf$fixVarToWeight<-1
conf$obsCorStruct[] <- c("ID", "ID", "ID", "AR", "ID")
#conf$keyCorObs[4,7:11]<-0
conf$keyCorObs[4,]<-c(-1 ,-1, -1 , 0  ,0 , 0,  0,  0,  0,   0 ,  0 , -1)

conf$keyVarF[1,]    <- c(0,1,rep(2,6),rep(-1,5))

conf$keyVarObs[1,]   <- c( 0,1,rep(2,11) )
conf$keyVarObs[2,1]  <- 3
conf$keyVarObs[3,1]  <- 4
conf$keyVarObs[4,]   <- c(-1, -1, -1,  5, 6,  6,  6,  6, 6,  6,  6,   6,  -1 )
conf$keyVarObs[5,]      <- c(-1,-1, 7, 7, 7, 7, 7, 7, 7,  7,  7,  7, -1)
confsave <-conf

# ACM specific parameters
#conf$keyAgeSampleData[]<-NA                       # no raw age reading used
#conf$keyXtraSd <- matrix(numeric(0),0,0)          # no sure what this is but needs to be  <0 x 0 matrix>
#conf$offsetAgeSampleData[] <- NA                  # only one offset defined, for the catches

par<-defpar(dat,conf)

fit.new0.02 <-sam.fit(dat,conf,par)
save(fit.new0.02, file="model fit ACMmean0.02.RData")


partable(fit.new0.02)             # no new parameters

load("model fit check.RData")
fit.new2
fit.new0.02         # slightly different

fits<-list(fit.new2,fit.new0.02)
class(fits) <- "samset"
ssbplot(fits)       # slightly different  
recplot(fits)


# model fitting : no raw data, weighted mean between externally fitted ACM  and ID   p=5%----------------------------------------------------------------
agerrors <- read.csv("../Mac SAM/data/2011 ageing errors.csv")
agerrors <- as.matrix(agerrors[,-1], dimnames =  list(paste("age",0:12),paste("age",0:12)))
apply(agerrors,1,sum)


wt.acm <- 0.10
ACMmean <- wt.acm * agerrors + (1-wt.acm)* diag(nrow=13)
apply(ACMmean,1,sum)


attr(cn,"ageConfusion")   <- ACMmean
dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))

#### model configuration
conf<-defcon(dat)
conf$keyLogFsta[1,] <- c(0,1,2,3,4,5,6,7,7,7,7,7,7)
conf$keyVarObs[4,]      <- c(-1,-1,-1, 3,4,4,4,4,4,4,4,4,-1)
conf$fbarRange <- c(4,8)
conf$corFlag <- 0
conf$fixVarToWeight<-1
conf$obsCorStruct[] <- c("ID", "ID", "ID", "AR", "ID")
#conf$keyCorObs[4,7:11]<-0
conf$keyCorObs[4,]<-c(-1 ,-1, -1 , 0  ,0 , 0,  0,  0,  0,   0 ,  0 , -1)

conf$keyVarF[1,]    <- c(0,1,rep(2,6),rep(-1,5))

conf$keyVarObs[1,]   <- c( 0,1,rep(2,11) )
conf$keyVarObs[2,1]  <- 3
conf$keyVarObs[3,1]  <- 4
conf$keyVarObs[4,]   <- c(-1, -1, -1,  5, 6,  6,  6,  6, 6,  6,  6,   6,  -1 )
conf$keyVarObs[5,]      <- c(-1,-1, 7, 7, 7, 7, 7, 7, 7,  7,  7,  7, -1)
confsave <-conf

# ACM specific parameters
#conf$keyAgeSampleData[]<-NA                       # no raw age reading used
#conf$keyXtraSd <- matrix(numeric(0),0,0)          # no sure what this is but needs to be  <0 x 0 matrix>
#conf$offsetAgeSampleData[] <- NA                  # only one offset defined, for the catches
par<-defpar(dat,conf)

fit.new0.04 <-sam.fit(dat,conf,par)
save(fit.new0.04, file="model fit ACMmean0.04.RData")


partable(fit.new0.04)            

load("model fit check.RData")
fit.new2
fit.new0.04        

fits<-list(fit.new2,fit.new0.04)
class(fits) <- "samset"
ssbplot(fits)       




#### - ---------------------------------------------------------------------------------
#### "correct" the catches externally by multiplying by inverse of ACM
#### - ---------------------------------------------------------------------------------
cn<-read.ices("data/cn.dat")


# with the fitted ACM
library(matlib)
cn2 <- inv((ACM.cn)) %*% t(cn)
cn2<-t(cn2)

colnames(cn2)  <- paste("age", 0:12)
colnames(cn)  <- paste("age", 0:12)

cndf  <- tidyr::gather(data.frame(year=1980:2020,cn), "age" , "catch" ,-year)
cndf2 <- tidyr::gather(data.frame(year=1980:2020,cn2), "age" , "catch" ,-year)
cndf$origin <- "original"
cndf2$origin <- "back transformed"

cdf <- rbind(cndf,cndf2)

library(ggplot2)
ggsave("../plots/backtransformedCatch_Mac_estimatedACM.png",
ggplot(cdf , aes(year,catch,colour = origin)) + geom_line() + facet_wrap(~factor(age,levels = ( paste("age.", 0:12,sep=""))))
)
colnames(cn)  <- as.character(0:12)

matpl<-ACM.cn
row.names(matpl) <- paste("true age",0:12)
colnames(matpl) <- paste("age read",0:12)
ggsave("../plots/ACM estimated Mackerel.png",
ggcorrplot(t(matpl),lab=T,title="ACM estimated Mackerel",legend.title = "Prop")
)


#### - ---------------------------------------------------------------------------------
#### "correct" the catches externally by multiplying by inverse of ACM   with the empirical ACM
#### - ---------------------------------------------------------------------------------

# with the empirical ACM
cn<-read.ices("data/cn.dat")
agerrors <- read.csv("../Mac SAM/data/2011 ageing errors.csv")
agerrors<-as.matrix(agerrors[,-1], dimnames =  list(paste("age",0:12),paste("age",0:12)))




library(matlib)
cn2 <- inv(t(agerrors)) %*% t(cn)
cn2<-t(cn2)
cn2save<-cn2


colnames(cn2)  <- paste("age", 0:12)
colnames(cn)  <- paste("age", 0:12)

cndf  <- tidyr::gather(data.frame(year=1980:2020,cn), "age" , "catch" ,-year)
cndf2 <- tidyr::gather(data.frame(year=1980:2020,cn2), "age" , "catch" ,-year)
cndf$origin <- "original"
cndf2$origin <- "back transformed"

cdf <- rbind(cndf,cndf2)

library(ggplot2)
ggsave("../plots/backtransformedCatch_Mac_empiricalACM.png",
ggplot(cdf , aes(year,catch,colour = origin)) + geom_line() + facet_wrap(~factor(age,levels = ( paste("age.", 0:12,sep=""))))
)

matpl<-agerrors
row.names(matpl) <- paste("true age",0:12)
colnames(matpl) <- paste("age read",0:12)
ggsave("../plots/empirical ACM Mackerel.png",
ggcorrplot(t(matpl),lab=T,title="empirical ACM Mackerel",legend.title = "Prop")
)

#check ACM
cn2save
colnames(cn2save)  <- paste(0:12)

# try with ID matrix 
W<-matrix(NA,nrow=nrow(cn2save), ncol=ncol(cn2save))
W[as.numeric(rownames(cn2save))<2000]<-10
attr(cn2save,"weight")<-W
attr(cn2save,"ageConfusion")   <- diag(nrow = 13)

dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn2save, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))

#### model configuration

conf<-defcon(dat)
conf$keyLogFsta[1,] <- c(0,1,2,3,4,5,6,7,7,7,7,7,7)
conf$keyVarObs[4,]      <- c(-1,-1,-1, 3,4,4,4,4,4,4,4,4,-1)
conf$fbarRange <- c(4,8)
conf$corFlag <- 0
conf$fixVarToWeight<-1
conf$obsCorStruct[] <- c("ID", "ID", "ID", "AR", "ID")
#conf$keyCorObs[4,7:11]<-0
conf$keyCorObs[4,]<-c(-1 ,-1, -1 , 0  ,0 , 0,  0,  0,  0,   0 ,  0 , -1)

conf$keyVarF[1,]    <- c(0,1,rep(2,6),rep(-1,5))

conf$keyVarObs[1,]   <- c( 0,1,rep(2,11) )
conf$keyVarObs[2,1]  <- 3
conf$keyVarObs[3,1]  <- 4
conf$keyVarObs[4,]   <- c(-1, -1, -1,  5, 6,  6,  6,  6, 6,  6,  6,   6,  -1 )
conf$keyVarObs[5,]      <- c(-1,-1, 7, 7, 7, 7, 7, 7, 7,  7,  7,  7, -1)
confsave <-conf

#conf$keyAgeSampleData[]<-NA                       # no raw age reading used
#conf$keyXtraSd <- matrix(numeric(0),0,0)          # no sure what this is but needs to be  <0 x 0 matrix>
#conf$offsetAgeSampleData[] <- NA                  # only one offset defined, for the catches

par<-defpar(dat,conf)


fit.new5 <-sam.fit(dat,conf,par)
partable(fit.new5)
save(fit.new5, file="model fit corrected catches.RData")


load("model fit check.RData")
fit.new2
fit.new5

plot(fit.new5)

fits<-list(fit.new5,fit.new2)
class(fits) <- "samset"
ssbplot(fits)           
catchplot(fits)           









