rm(list=ls())

# devtools::install_github("fishfollower/SAM/stockassessment")
library(stockassessment)


# read in data -----------------------------------------------------------------
source("datascript.R")

#recap$splitPhi=rep(1,nrow(recap))    # that is actually part of model configuration, but not included in the conf file


recap<-recap[recap$Type==1 & recap$RecaptureY<=2006,]



dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                   # recapture=recap,
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))

#### model configuration

conf<-defcon(dat)
conf$keyLogFsta[1,] <- c(0,1,2,3,4,5,6,7,7,7,7,7,7)
conf$keyVarObs[4,]      <- c(-1,-1,-1, 3,4,4,4,4,4,4,4,4,-1)
conf$fbarRange <- c(4,8)
conf$corFlag <- 0
conf$fixVarToWeight<-1
conf$obsCorStruct[] <- c("ID", "ID", "ID", "AR", "ID")
#conf$keyCorObs[4,7:11]<-0
conf$keyCorObs[4,]<-c(-1 ,-1, -1 , 0  ,0 , 0,  0,  0,  0,   0 ,  0 , -1)

conf$keyVarF[1,]    <- c(0,1,rep(2,6),rep(-1,5))

conf$keyVarObs[1,]   <- c( 0,1,rep(2,11) )
conf$keyVarObs[2,1]  <- 3
conf$keyVarObs[3,1]  <- 4
conf$keyVarObs[4,]   <- c(-1, -1, -1,  5, 6,  6,  6,  6, 6,  6,  6,   6,  -1 )
conf$keyVarObs[5,]      <- c(-1,-1, 7, 7, 7, 7, 7, 7, 7,  7,  7,  7, -1)

par<-defpar(dat,conf)




# model fitting ----------------------------------------------------------------
#fit.new <-sam.fit(dat,conf,par, newtonsteps=0, map=list(logitRecapturePhi=factor(c(1,1))))
# don't forget to remove the newtonsteps = 0
fit.new2 <-sam.fit(dat,conf,par)
save(fit.new2, file="model fit check.RData")

load("model fit.RData")
fit.new
fit.new2

plot(fit.new)

fits<-list(fit.new,fit.new2)
class(fits) <- "samset"
ssbplot(fits)             # different at the start, pretty close after