rm(list=ls())

# devtools::install_github("fishfollower/SAM/stockassessment")
library(stockassessment)


setwd("M:/WGWIDE/WGWIDE2021/assessment/WGWIDE2021 tag index/")

# read in data -----------------------------------------------------------------
source("datascript.R")

#recap$splitPhi=rep(1,nrow(recap))    # that is actually part of model configuration, but not included in the conf file


recap<-recap[recap$Type==1 & recap$RecaptureY<=2006,]



dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    recapture=recap)

#### model configuration

conf<-defcon(dat)
conf$keyLogFsta[1,] <- c(0,1,2,3,4,5,6,7,7,7,7,7,7)
conf$keyVarObs[4,]      <- c(-1,-1,-1, 3,4,4,4,4,4,4,4,4,-1)
conf$fbarRange <- c(4,8)
conf$corFlag <- 0
conf$fixVarToWeight<-1
conf$obsCorStruct[] <- c("ID", "ID", "ID", "AR", "ID","ID")
#conf$keyCorObs[4,7:11]<-0
conf$keyCorObs[4,]<-c(-1 ,-1, -1 , 0  ,0 , 0,  0,  0,  0,   0 ,  0 , -1)

conf$keyVarF[1,]    <- c(0,1,rep(2,6),rep(-1,5))

conf$keyVarObs[1,]   <- c( 0,1,rep(2,11) )
conf$keyVarObs[2,1]  <- 3
conf$keyVarObs[3,1]  <- 4
conf$keyVarObs[4,]   <- c(-1, -1, -1,  5, 6,  6,  6,  6, 6,  6,  6,   6,  -1 )
conf$keyVarObs[5,]      <- c(-1,-1, 7, 7, 7, 7, 7, 7, 7,  7,  7,  7, -1)

par<-defpar(dat,conf)




# model fitting ----------------------------------------------------------------
#fit.new <-sam.fit(dat,conf,par, newtonsteps=0, map=list(logitRecapturePhi=factor(c(1,1))))
# don't forget to remove the newtonsteps = 0
fit.new <-sam.fit(dat,conf,par)

save(fit.new, file="model fit.RData")


# retrospective ----------plot------------------------------------------------------
ret<-retro(fit.new, year=6)
ret7<-retro(fit.new, year=matrix(c(2015,2016,2015,2016,2014)-1, ncol=5), map=list(logitRecapturePhi=factor(c(1))))
ret[[7]]<-ret7[[1]]

ret5peels<-retro(fit.new, year=5)

mohn(ret)

# leave one out ----------------------------------------------------------------
lo<-leaveout(fit.new, fleet=c(2,3,4,5))
lo2<-lo

save(lo2,file="diags.RData")
#NB: Theis is OK: Warning message:
   #In sam.fit(data, conf, par, rm.unidentified = TRUE, map = map, lower = fit$low,  :
             #Initial values are not consistent, so running with default init values from defpar()


# residuals --------------------------------------------------------------------
options(mc.cores=4)

dat<-fit.new$data
dis<-dat$fleetTypes[dat$aux[,"fleet"]]==5
res<-TMB::oneStepPredict(fit.new$obj, observation.name="logobs", data.term.indicator="keep", discrete=FALSE, subset=which(!dis), parallel=TRUE)
res2<-TMB::oneStepPredict(fit.new$obj, observation.name="logobs", data.term.indicator="keep", discrete=TRUE, conditional=which(!dis),
                          subset=which(dis), method ="oneStepGeneric", range=c(0,Inf), parallel=TRUE)

totalres<-rep(NA,nrow(dat$aux))
totalres[!dis]<-res$residual
totalres[dis]<-res2$residual
myres<-data.frame(dat$aux, residual=totalres)

TAGRES<-myres[myres$fleet==5,]
myres$residual[myres$fleet==5]<-NA
class(myres)<-"samres"
attr(myres, "fleetNames")<-attr(fit.new$data, "fleetNames")
RES<-myres
RESP<-procres(fit.new)

save(RESP,RES,TAGRES,ret,ret5peels,lo2,file="diags.RData")




 load("diags.RData")
# produce plots of the diagnostics ---------------------------------------------
pdf("diagnostics.pdf", 8, 10)
ssbplot(fit.new)
fbarplot(fit.new)
recplot(fit.new)
catchplot(fit.new)
fitplot(fit.new, fleet=1)
fitplot(fit.new, fleet=2, ylim=c(14.5, 16))
fitplot(fit.new, fleet=3)
fitplot(fit.new, fleet=4)
source("fitplot.tags.r")
fitplot.tags(fit.new)
plot(RESP)
plotby(TAGRES$year, TAGRES$RecaptureY-TAGRES$year, TAGRES$residual, by=TAGRES$age, xlab="Year", ylab="No years out"); title(paste(round(range(TAGRES$residual),1), collapse=" - "))
plot(RES)

ssbplot(ret,main="retro SSB")
fbarplot(ret,partial=F,main="retro Fbar")
recplot(ret,main = "retro Recruitment" , las=0, drop=1)
catchplot( ret, main = "retro Catch")

ssbplot(lo2,main="leave one out SSB")
fbarplot(lo2,partial=F,main="leave one out Fbar")
recplot(lo2,main = "leave one out Recruitment" , las=0, drop=1)
catchplot( lo2, main = "leave one out Catch")

dev.off()


