  rm(list=ls())

# devtools::install_github("fishfollower/SAM/stockassessment")
library(stockassessment)


# read in data 
source("datascript.R")

load("data/ACM Mac.RData")
ACM.cn <- ACM.cn[3:13,]
ACM.cn[,3] <- apply(ACM.cn[,1:3],1,sum)
ACM.cn   <- ACM.cn[,3:13]
ACM.cn <- structure(as.vector((ACM.cn)),.Dim = c(11L, 11L))
apply(ACM.cn,1,sum)
#### - ---------------------------------------------------------------------------------
#### run without ACM, starting at age2 and using the tags as index
#### - ---------------------------------------------------------------------------------

dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))

# read in model configuration 
conf<-defcon(dat)
conf$keyLogFsta[1,] <- c(0,1,2,3,4,5,5,5,5,5,5)

conf$fbarRange <- c(4,8)
conf$corFlag <- 0
conf$fixVarToWeight<-1
conf$obsCorStruct[] <- c("ID",  "ID", "AR", "ID")
#conf$keyCorObs[4,7:11]<-0
conf$keyCorObs[3,]<-c( -1 , 0  ,0 , 0,  0,  0,  0,   0 ,  0 , -1)
conf$keyVarObs[3,]   <- c( -1,  2, 3,  3,  3,  3, 3,  3,  3,   3,  -1 )
conf$keyVarObs[4,]   <- c(-1,-1,-1,4,4,4,4,4,4,4,-1)

conf$keyLogFpar[4,]   <- c(-1,-1,-1,9,9,9,9,9,9,9,-1)
confsave <-conf
par<-defpar(dat,conf)


# model fitting 
fit.age2tagidx <-sam.fit(dat,conf,par)
save(fit.age2tagidx , file="model fit age2 tag index.RData")

load("model fit.RData")
fit.age2tagidx 
fit.age2

fits<-list(fit.age2,fit.age2tagidx)
class(fits) <- "samset"
ssbplot(fits)   








#### - ---------------------------------------------------------------------------------
#### run with the ACM estimated externally, starting at age2 and using the tags as index
#### - ---------------------------------------------------------------------------------

# model fitting : no raw data, ACM fitted externally ----------------------------------------------------------------
p<-0.01 # works
p<-0.04 # works
p<-0.05  # doesn't work



attr(cn,"ageConfusion")   <- p * ACM.cn + (1-p) * diag(nrow=11)

apply(attributes(cn)$ageConfusion,1,sum)

dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))
                    
conf <- confsave                   
                   
par<-defpar(dat,conf)


# model fitting 
fit.ACM <-sam.fit(dat,conf,par)
save(fit.ACM , file="model fit age2 tag index.RData")
                    

#### - ---------------------------------------------------------------------------------
#### run with the empirical ACM based on modal age, starting at age2 and using the tags as index
#### - ---------------------------------------------------------------------------------
agerrors <- read.csv("../Mac SAM/data/2011 ageing errors.csv")
agerrors <- as.matrix(agerrors[,-1], dimnames =  list(paste("age",0:12),paste("age",0:12)))
agerrors <- agerrors[3:13,]
agerrors[,3] <- apply(agerrors[,1:3],1,sum)
agerrors     <- agerrors[,3:13]
apply(agerrors,1,sum)

 
# model fitting : no raw data, ACM fitted externally ----------------------------------------------------------------
p<-0.01 # works
p<-0.03 # works
p<-0.04 # works # doesn't work
p<-0.5  # doesn't work


attr(cn,"ageConfusion")   <- p * agerrors + (1-p) * diag(nrow=11)
dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))
                    
conf <- confsave                   
                   
par<-defpar(dat,conf)


# model fitting 
fit.ACM <-sam.fit(dat,conf,par)
save(fit.ACM , file="model fit age2 tag index.RData")

                    
                    
#### - ---------------------------------------------------------------------------------
#### "correct" the catches externally by multiplying by inverse of ACM
#### - ---------------------------------------------------------------------------------

library(matlib)
cn2 <- inv(t(ACM.cn)) %*% t(cn)
cn2<-t(cn2)

colnames(cn2)  <- paste("age", 2:12)
colnames(cn)  <- paste("age", 2:12)

cndf  <- tidyr::gather(data.frame(year=1980:2020,cn), "age" , "catch" ,-year)
cndf2 <- tidyr::gather(data.frame(year=1980:2020,cn2), "age" , "catch" ,-year)
cndf$origin <- "original"
cndf2$origin <- "back transformed"

cdf <- rbind(cndf,cndf2)

library(ggplot2)
ggplot(cdf , aes(year,catch,colour = origin)) + geom_line() + facet_wrap(~factor(age,levels = ( paste("age.", 0:12,sep=""))))

# takes crazy values


#### - ---------------------------------------------------------------------------------
#### "correct" the catches externally by multiplying by inverse of ACM   with the empirical ACM
#### - ---------------------------------------------------------------------------------
library(matlib)
cn2 <- inv(t(agerrors)) %*% t(cn)
cn2<-t(cn2)
cn2save<-cn2


colnames(cn2)  <- paste("age", 2:12)
colnames(cn)  <- paste("age", 2:12)

cndf  <- tidyr::gather(data.frame(year=1980:2020,cn), "age" , "catch" ,-year)
cndf2 <- tidyr::gather(data.frame(year=1980:2020,cn2), "age" , "catch" ,-year)
cndf$origin <- "original"
cndf2$origin <- "back transformed"

cdf <- rbind(cndf,cndf2)

library(ggplot2)
ggplot(cdf , aes(year,catch,colour = origin)) + geom_line() + facet_wrap(~factor(age,levels = ( paste("age.", 0:12,sep=""))))


cn2save
colnames(cn2save)  <- paste(2:12)

# try with ID matrix 
W<-matrix(NA,nrow=nrow(cn2save), ncol=ncol(cn2save))
W[as.numeric(rownames(cn2save))<2000]<-10
attr(cn2save,"weight")<-W
attr(cn2save,"ageConfusion")   <- diag(nrow = 11)


 
dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn2save, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))

# read in model configuration 
conf<-defcon(dat)
conf$keyLogFsta[1,] <- c(0,1,2,3,4,5,5,5,5,5,5)

conf$fbarRange <- c(4,8)
conf$corFlag <- 0
conf$fixVarToWeight<-1
conf$obsCorStruct[] <- c("ID",  "ID", "AR", "ID")
#conf$keyCorObs[4,7:11]<-0
conf$keyCorObs[3,]<-c( -1 , 0  ,0 , 0,  0,  0,  0,   0 ,  0 , -1)
conf$keyVarObs[3,]   <- c( -1,  2, 3,  3,  3,  3, 3,  3,  3,   3,  -1 )
conf$keyVarObs[4,]   <- c(-1,-1,-1,4,4,4,4,4,4,4,-1)

conf$keyLogFpar[4,]   <- c(-1,-1,-1,9,9,9,9,9,9,9,-1)
confsave <-conf
par<-defpar(dat,conf)


# model fitting 
fit.age2tagidx <-sam.fit(dat,conf,par)
save(fit.age2tagidx , file="model fit age2 tag index.RData")

load("model fit.RData")
fit.age2tagidx 
fit.age2

fits<-list(fit.age2,fit.age2tagidx)
class(fits) <- "samset"
ssbplot(fits)   








                   