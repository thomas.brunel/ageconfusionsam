rm(list=ls())

# devtools::install_github("fishfollower/SAM/stockassessment")
library(stockassessment)


setwd("M:/WGWIDE/WGWIDE2021/assessment/age2/")

# read in data -----------------------------------------------------------------
source("datascript.R")

#recap$splitPhi=rep(1,nrow(recap))    # that is actually part of model configuration, but not included in the conf file

dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    recapture=recap)

# read in model configuration --------------------------------------------------
source('configuration.r')
par<-defpar(dat,conf)

# model fitting ----------------------------------------------------------------
#fit.new <-sam.fit(dat,conf,par, newtonsteps=0, map=list(logitRecapturePhi=factor(c(1,1))))
# don't forget to remove the newtonsteps = 0
fit.age2 <-sam.fit(dat,conf,par,  map=list(logitRecapturePhi=factor(c(1,1))))

save(fit.age2, file="model fit.RData")

fit.new<-fit.age2
# retrospective ----------plot------------------------------------------------------
ret<-retro(fit.new, year=6)
ret7<-retro(fit.new, year=matrix(c(2015,2016,2016,2014)-1, ncol=4), map=list(logitRecapturePhi=factor(c(1))))
ret[[7]]<-ret7[[1]]

mohn(ret)

# leave one out ----------------------------------------------------------------
fit.new.wo<-runwithout(fit.new, year=2013:2019, fleet=c(4,4,4,4,4,4), map=list(logitRecapturePhi=factor(c(1))))
# fit.new.wo2<-runwithout(fit.new, year=2013:2018, fleet=c(5,5,5,5,5), map=list(logitRecapturePhi=factor(c(1)), logSdLogFsta=factor(c(1,2,2))))
lo<-leaveout(fit.new, fleet=c(2,3))
lo$"w.o. RFID"<-fit.new.wo

#NB: Theis is OK: Warning message:
   #In sam.fit(data, conf, par, rm.unidentified = TRUE, map = map, lower = fit$low,  :
             #Initial values are not consistent, so running with default init values from defpar()


# residuals --------------------------------------------------------------------
options(mc.cores=4)

dat<-fit.new$data
dis<-dat$fleetTypes[dat$aux[,"fleet"]]==4
res<-TMB::oneStepPredict(fit.new$obj, observation.name="logobs", data.term.indicator="keep", discrete=FALSE, subset=which(!dis), parallel=TRUE)
res2<-TMB::oneStepPredict(fit.new$obj, observation.name="logobs", data.term.indicator="keep", discrete=TRUE, conditional=which(!dis),
                          subset=which(dis), method ="oneStepGeneric", range=c(0,Inf), parallel=TRUE)

totalres<-rep(NA,nrow(dat$aux))
totalres[!dis]<-res$residual
totalres[dis]<-res2$residual
myres<-data.frame(dat$aux, residual=totalres)

TAGRES<-myres[myres$fleet==4,]
myres$residual[myres$fleet==4]<-NA
class(myres)<-"samres"
attr(myres, "fleetNames")<-attr(fit.new$data, "fleetNames")
RES<-myres
RESP<-procres(fit.new)

save(RESP,RES,TAGRES,ret,lo,file="diags.RData")

 load("diags.RData")
# produce plots of the diagnostics ---------------------------------------------
pdf("diagnostics.pdf", 8, 10)
ssbplot(fit.new)
fbarplot(fit.new)
recplot(fit.new)
catchplot(fit.new)
fitplot(fit.new, fleet=1)
fitplot(fit.new, fleet=2, ylim=c(14.5, 16))
fitplot(fit.new, fleet=3)
source("fitplot.tags.r")
fitplot.tags(fit.new)
plot(RESP)
plotby(TAGRES$year, TAGRES$RecaptureY-TAGRES$year, TAGRES$residual, by=TAGRES$age, xlab="Year", ylab="No years out"); title(paste(round(range(TAGRES$residual),1), collapse=" - "))
plot(RES)

ssbplot(ret,main="retro SSB")
fbarplot(ret,partial=F,main="retro Fbar")
recplot(ret,main = "retro Recruitment" , las=0, drop=1)
catchplot( ret, main = "retro Catch")

ssbplot(lo,main="leave one out SSB")
fbarplot(lo,partial=F,main="leave one out Fbar")
recplot(lo,main = "leave one out Recruitment" , las=0, drop=1)
catchplot( lo, main = "leave one out Catch")

dev.off()


