
cat("!! please check : some values are hardcoded and need to be updated each year \n")

cn<-read.ices("data/cn.dat")
cw<-read.ices("data/cw.dat")
dw<-read.ices("data/dw.dat")
lf<-read.ices("data/lf.dat")
lw<-read.ices("data/lw.dat")
mo<-read.ices("data/mo.dat")
nm<-read.ices("data/nm.dat")
pf<-read.ices("data/pf.dat")
pm<-read.ices("data/pm.dat")
sw<-read.ices("data/sw.dat")
surveys<-read.ices("data/survey.dat")

recap<-read.table("data/tag_steel.dat", header=TRUE)
recap<-recap[recap$Type==1 & recap$RecaptureY<=2006,]
recap<-recap[recap[,1]>=min(as.numeric(rownames(sw))), ]

recap2<-read.table("data/tag_RFID.dat", header=TRUE)
recap2$r<-round(recap2$r)
recap2<-recap2[recap2$Nscan>0,]
age <- recap2$ReleaseY - recap2$Yearclass
# subset of the data according to changes made in IBP 2019
recap2 <- recap2[age>4,]
recap2 <- recap2[(recap2$RecaptureY-recap2$ReleaseY) %in% c(1,2), ]
recap2 <- recap2[recap2$ReleaseY>=2013, ]

recap<-rbind(recap,recap2)

age<- recap$ReleaseY - recap$Yearclass
recap  <- recap[age>1 & age<12,]     # possibly test id <13 ok???

#### Remove the tags recapture in the release year
recap <- recap[recap$ReleaseY!=recap$RecaptureY,]

recap <- recap[recap$RecaptureY!=2021,]     # remove scans of catch in the current Y



W<-matrix(NA,nrow=nrow(cn), ncol=ncol(cn))
W[as.numeric(rownames(cn))<2000]<-10
attr(cn,"weight")<-W



# from the otolith workshop 
ACM.cn<-  read.csv("./data/2011 ageing errors.csv")
ACM.cn <- structure(as.vector(t(ACM.cn)[-1,]),.Dim = c(13L, 13L))

# or modelled
load("../../ageConfusionExample/ACM Mac.RData")
ACM.cn <- structure(as.vector(t(ACM.cn)),.Dim = c(13L, 13L))
attr(cn,"ageConfusion")<-ACM.cn





raw <- read.csv("./data/rawExperts2011.csv")
raw$sample <- 1:dim(raw)[1]
raw<- tidyr::gather(raw,key=data,value=age,1:c(dim(raw)[2]-1))
raw <- subset(raw ,!is.na(raw$age))
raw <- raw[order(raw[,1]),]
raw$data<-1
raw<-raw[,c("age","sample","data")]

rawmed<-aggregate(age ~ sample, raw , median)
names(rawmed)[2] <- "medage"
rawplot <- merge(raw , rawmed , all.x=T)
library(ggplot2)
png("./data/2011 otolith exchange.png")
print(ggplot(rawplot  , aes(age,fill=medage)) + geom_histogram(binwidth=1) + facet_wrap(~medage))
dev.off()


# create a fake one, with way less error that the real mackerel
raw2base<-aggregate(age~sample,raw, median)
#raw2<-lapply(as.list(1:max(raw2$sample)),function(x) sample(raw2$age[raw2$sample == x]+c(-1:1),15,replace =T, prob=c(1,8,1)))
alfa <-   0.01
beta <-   2
raw2<-lapply(as.list(1:max(raw2base$sample)),function(x) round(rnorm(n = 15 , mean = raw2base$age[raw2base$sample == x] , sd = 0.3 + alfa * raw2base$age[raw2base$sample == x] ^beta ),0))
raw2 <-data.frame(do.call("rbind",raw2))
raw2$sample <- 1:dim(raw2)[1]
raw2<- tidyr::gather(raw2,key=data,value=age,1:c(dim(raw2)[2]-1))
raw2 <- subset(raw2 ,!is.na(raw2$age))
raw2 <- raw2[order(raw2[,1]),]
raw2$data<-1
raw2<-raw2[,c("age","sample","data")]

names(raw2base)[2] <- "meanage"
raw3 <- merge(raw2 , raw2base ,all.x=T)
png("./data/sim otolith exchange.png")
print(ggplot(raw3  , aes(age,fill=meanage)) + geom_histogram(binwidth=1) + facet_wrap(~meanage))
dev.off()
