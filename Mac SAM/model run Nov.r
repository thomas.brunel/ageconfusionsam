rm(list=ls())
library(stockassessment)


###### age confusion with estimated error matrix (from otolith exchange readings)######
#######################################################################################
# read in data -----------------------------------------------------------------
source("datascript.R")
dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    recapture=recap  #,
#                    agesampledata=raw     # real data (see "./data/2011 otolith exchange.png")
#                    agesampledata=raw2    # fake example (see  "./data/sim otolith exchange.png")
                                        )

# model configuration --------------------------------------------------
source('configuration.r')
conf$keyAgeSampleData[1]  <-    1
conf$offsetAgeSampleData[1] <- -1
conf$keyXtraSd <- matrix(numeric(0),0,0)

par<-defpar(dat,conf)

# model fitting 
fit1 <-sam.fit(dat,conf,par,  map=list(logitRecapturePhi=factor(c(1,1))))








###### age confusion with EXTERNAL error matrix (try with perfect age reading = identity matrix)######
#######################################################################################
source("datascript.R")
attr(cn,"ageConfusion")<-diag(13)

dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    recapture=recap,
#                    agesampledata=raw     # real data (see "./data/2011 otolith exchange.png")
#                    agesampledata=raw2    # fake example (see  "./data/sim otolith exchange.png")
                    agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0))     # to remove all age reading data, but when having    
                                                                                                # conf$keyAgeSampleData = NA , age reading is not used anyway
                                                                                                # It tested both on fit2, it makes no difference
                                    )

# read in model configuration --------------------------------------------------
source('configuration.r')
par<-defpar(dat,conf)

# model fitting ----------------------------------------------------------------
fit2 <-sam.fit(dat,conf,par,  map=list(logitRecapturePhi=factor(c(1,1))))

# compare with WGWIDE 2021
load("model fit WGWIDE21.RData")
fit.new
fit2


###### age confusion with atual EXTERNAL error matrix ######
#######################################################################################
 # read in data -----------------------------------------------------------------
source("datascript.R")
attr(cn,"ageConfusion")<-ACM.cn


dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    recapture=recap,
#                    agesampledata=raw     # real data (see "./data/2011 otolith exchange.png")
#                    agesampledata=raw2    # fake example (see  "./data/sim otolith exchange.png")
                      agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)) 
                                        )

# read in model configuration --------------------------------------------------
source('configuration.r')
par<-defpar(dat,conf)

# model fitting ----------------------------------------------------------------
fit3 <-sam.fit(dat,conf,par,  map=list(logitRecapturePhi=factor(c(1,1))))




###### age confusion with a fake "cleaner" EXTERNAL error matrix ######
#######################################################################################
 # read in data -----------------------------------------------------------------
source("datascript.R")
ACMclean.cn<-  read.csv("./data/2011 ageing errors FAKE small.csv")
ACMclean.cn <- structure(as.vector(t(ACMclean.cn)[-1,]),.Dim = c(13L, 13L))
attr(cn,"ageConfusion")<-ACMclean.cn


dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    recapture=recap,
#                    agesampledata=raw     # real data (see "./data/2011 otolith exchange.png")
#                    agesampledata=raw2    # fake example (see  "./data/sim otolith exchange.png")
                      agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)) 
                                        )

# read in model configuration --------------------------------------------------
source('configuration.r')
par<-defpar(dat,conf)

# model fitting ----------------------------------------------------------------
fit4 <-sam.fit(dat,conf,par,  map=list(logitRecapturePhi=factor(c(1,1))))
plot(c(fit.new,fit4))


###### age confusion with a fake "dirtier" EXTERNAL error matrix ######
#######################################################################################
 # read in data -----------------------------------------------------------------
source("datascript.R")
ACMclean.cn<-  read.csv("./data/2011 ageing errors FAKE med.csv")
ACMclean.cn <- structure(as.vector(t(ACMclean.cn)[-1,]),.Dim = c(13L, 13L))
attr(cn,"ageConfusion")<-ACMclean.cn


dat<-setup.sam.data(surveys=surveys,
                    residual.fleet=cn, 
                    prop.mature=mo, 
                    stock.mean.weight=sw, 
                    catch.mean.weight=cw, 
                    dis.mean.weight=dw, 
                    land.mean.weight=lw,
                    prop.f=pf, 
                    prop.m=pm, 
                    natural.mortality=nm, 
                    land.frac=lf,
                    recapture=recap,
#                    agesampledata=raw     # real data (see "./data/2011 otolith exchange.png")
#                    agesampledata=raw2    # fake example (see  "./data/sim otolith exchange.png")
                      agesampledata=cbind(age=numeric(0), sample=numeric(0), data=numeric(0)) 
                                        )

# read in model configuration --------------------------------------------------
source('configuration.r')
par<-defpar(dat,conf)

# model fitting ----------------------------------------------------------------
fit5 <-sam.fit(dat,conf,par,  map=list(logitRecapturePhi=factor(c(1,1))))
plot(c(fit.new,fit4))

