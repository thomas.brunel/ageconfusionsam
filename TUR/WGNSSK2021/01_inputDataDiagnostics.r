#-------------------------------------------------------------------------------
# Re-run 2019 assessment
# Code to generate the .dat file for input into the ADMB model
# Uses Lowestoft format input files
# David Miller, some code adapted from FLR-project
# Date: 3 Oct 2012
#-------------------------------------------------------------------------------
rm(list=ls())

# Install FLR-packages
# install.packages(pkgs="FLAssess",repos="http://flr-project.org/R")
# install.packages(pkgs="FLEDA",repos="http://flr-project.org/R")
# install.packages(pkgs="FLCore",repos="http://flr-project.org/R")
# install.packages("FLSAM", repos="http://flr-project.org/R")  
# install Rtools40
# devtools::install_github("fishfollower/SAM/stockassessment", ref="components")
# install.packages('TMB', type = 'source')

# Load libraries
library(FLCore);library(mgcv)
library(FLAssess); library(stockassessment)
library(FLEDA); library(splines); 
library(scales); library(gplots);library(grid); library(gridExtra); library(latticeExtra)
library(sas7bdat); library(RColorBrewer)

assYear <- 2021
stk <- "Turbot"
stk1 <- "TUR"

# Set paths to folders
#Path       <- paste0("W:/IMARES/Data/ICES-WG/WGNSSK/",AssYear,"/stocks/",stk,"/")
Path        <-  paste0("C:/Users/batsl001/OneDrive - WageningenUR/Bestandsbeheer/WGNSSK/",assYear,"/",stk,"/")

dataPath   <- paste(Path,"04_Lowestoft_files/",sep="")
outPath    <- paste(Path,"05_Assessment/Figures/",sep="")
sourcePath <- paste(Path,"05_assessment/source/",sep="")

## Source methods/functions
source(paste(sourcePath,"nsea_functions.r",sep=""))

### ------------------------------------------------------------------------------------------------------
###  1. Settings
### ------------------------------------------------------------------------------------------------------

## Assessment settings
## Stock name
stock_Name      <- "tur-nsea"
# Year (= year when assessment conducted.  i.e. have data up to assYear-1)
assYear         <- 2021
retroYr         <- 2021
endYear         <- min(assYear,retroYr)-1
Units           <- c("tonnes","thousands","kg")     # totals (product of the next two), numbers, weights-at-age
maxA            <- 10
pGrp            <- T
minFbar         <- 2
maxFbar         <- 6

### ------------------------------------------------------------------------------------------------------
###   2. Read and process assessment input data
### ------------------------------------------------------------------------------------------------------

## Read stock data
stock               <- readFLStock(paste(dataPath,"index.txt", sep=""))
units(stock)[1:17]  <- as.list(c(rep(Units,4), "NA", "NA", "f", "NA", "NA"))

# Change old ages landings to 0 (instead of NA) to allow plusgroup weight calculation
# Calculate plusgroup weight for stock
stkWtPgrp           <- stock.wt(stock)[maxA,]
# Apply max age
if (pGrp) stock     <- setPlusGroup(stock, plusgroup=maxA) else stock <- trim(stock, age=1:maxA)
# Trim if doing retro
stock.wt(stock)[maxA,] <- stkWtPgrp

# Number of years
years               <- as.numeric(range(stock)["minyear"]:range(stock)["maxyear"])
numYr               <- as.numeric(range(stock)["maxyear"]-range(stock)["minyear"]+1)
# Number of ages
numAges             <- length(1:maxA)
startyr             <- range(stock)[["minyear"]]## Read index data

# NOTE: all index values should run to (assYear-1) - because only #yrs read in, so start of index is calculated back from (assYear-1) and this.
indices             <- readFLIndices(paste(dataPath,"fleet.txt", sep=""), na.strings="-1")
indices             <- FLIndices(list(indices[[1]],indices[[2]],indices[[5]]))
indexVals           <- lapply(indices, index)
numIndices          <- length(indexVals)
indMPs              <- list()
for(ind in names(indices))
  indMPs[[ind]]     <- as.numeric((range(indices[[ind]])["startf"]+range(indices[[ind]])["endf"])/2)

disc <- read.table(paste0(dataPath,"diton.txt"))
disc <- disc[-1,]
colnames(disc) <- c("year","discards")
str(disc)
disc$discards <- as.numeric(as.character(disc$discards))
disc$Year <- as.numeric(as.character(disc$year))
### ------------------------------------------------------------------------------------------------------
###   2. make new plots
### ------------------------------------------------------------------------------------------------------

#Landings
plot(x=startyr:(assYear-1), y=landings(stock)/1000, xlim=c(1957,(assYear-1)), ylim=c(0,1.1*max(landings(stock)))/1000, type="l", xlab="Year",lty=1, ylab= "Landings ('000 t)",main="Landings", las=1, lwd=2, col="blue")
grid()

#Landings and discards (manually added discards, which are in a diton file in lowestoft files and come from intercatch)
tiff(filename = paste0(outPath,"Landings_and_discards.tif"),units = "cm", width = 20,height = 16,
     pointsize = 12,res = 150)
plot(x=startyr:(assYear-1), y=landings(stock)/1000, xlim=c(1975,(assYear-1)), ylim=c(0,1.1*max(landings(stock)))/1000, type="l", xlab="Year",lty=1, ylab= "Landings ('000 t)",main="Total landings and discards", las=1, lwd=2, col="blue")
lines(x=disc$Year[1]:(assYear-1), y=disc$discards/1000, col="red", lwd=2)
grid()
dev.off()

# as barchart
Data <- as.data.frame(landings(stock))
Data <- Data[,c(2,7)]
Data <- merge(Data,disc,by="year", all.x=T)
colnames(Data)[2] <- "landings"
Data$discards <- Data$discards
Data$Year <- NULL

library(reshape2);library(ggplot2)
Data <- melt(Data, id.vars = "year")
Data$cut <- factor(Data$variable, levels=c("discards", "landings"))
  
tiff(filename = paste0(outPath,"Landings_and_discards_barchart.tif"),units = "cm", width = 24,height = 16,
     pointsize = 12,res = 150)

ggplot(data=Data[Data$year > 1980,], aes(x=year,y=value,fill=cut))+
  geom_bar(stat="identity", color="black") +
  scale_fill_manual(values=c("#f03b20","#9BCD9B")) +
  scale_y_continuous(limits=c(0,8000),breaks=seq(0,8000,1000),expression("Catch in tonnes"),expand = c(0,0)) +
  scale_x_continuous(limits=c(1980,assYear),breaks=seq(1981,assYear,3),expand = c(0,0)) +
  theme(strip.text        = element_text(face="bold",size=12),
        strip.background  = element_rect(fill="white",colour="black",size=0.3),
        panel.grid.major  = element_line(colour="white"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=12),
        axis.text.y       = element_text(colour="black", size=12),
        axis.title.y      = element_text(angle=90, size=14),
        axis.title.x      = element_text(size=14),
        legend.position   = " ",
        legend.text       = element_text(colour="black", size=12 ),
        legend.title      = element_text(face="bold",colour="black", size=14 ),
        legend.key        = element_rect(fill = "white"),
        legend.key.size   = unit(1, "cm"),
        panel.spacing.x   = unit(1.5, "lines"),
        panel.spacing.y   = unit(0.9, "lines"),
        plot.margin       = unit(c(0.4,1.1,0.1,1.1), "cm"))
dev.off()

#Bubble plot of Landings at age
bsize <- 0.08 
plc <-  (landings.n(stock))[,ac(1975: (assYear-1))]
# bubbles(age~year, data=resL, col=c("black","black"), bub.scale=10, pch=c(21,21), fill=resL>0, xlim=c(1957,(assYear-1)), ylim=c(0,(maxA+1)), ylab= "Age", main="Landings residuals")
nmages <-  length(dimnames(plc)[[1]])
ylims <- 0.5*nmages+0.25
tiff(filename = paste0(outPath,"Landings_at_age.tif"),units = "cm", width = 20,height = 16,
     pointsize = 12,res = 150)
plot(NA,NA,main="Landings at age", xlab="Year", ylab="Age",xlim=c(as.numeric(min(dimnames(plc)[[2]])),as.numeric(max(dimnames(plc)[[2]]))), yaxt="n", ylim=c(0,ylims))
axis(2,at=seq(0.5,0.5*nmages,by=0.5),labels=as.numeric(dimnames(plc)[[1]]))
for (i in as.numeric(dimnames(plc)[[1]])) {
  radius <- as.numeric(sqrt(abs(plc[i,]) / pi)) *bsize
  points(dimnames(plc)[[2]], rep(0.5*i,ncol(plc)),cex=radius*2, pch=21, col=c("black", "blue")[1+as.numeric(plc[i,]>0)], bg=alpha(c("black", "blue")[1+as.numeric(plc[i,]>0)],0.5))
}
text(2005,0.1,paste("min = ",round(min(c(plc)[!is.infinite(c(plc))],na.rm=T),2),"; max = ",round(max(c(plc)[!is.infinite(c(plc))],na.rm=T),2) ,sep=""), cex=1, pos=4)
dev.off()

#z from catch curves (based on landings)

#Surveys
#SNS
plot(indices[[1]])
# BTS-ISIS
plot(indices[[2]])
# NL_LPUE
plot(indices[[3]])


#### Run indices diagnostics
idxcrop <-  indices
indsN01 <- FLQuants(lapply( mcf( lapply(idxcrop, index)), function(x){x <- FLQuant(aperm(apply(x@.Data, c(1,3,4,5,6), scale),c(2,1,3,4,5,6)), dimnames= dimnames(x))}))

names(indsN01)   <- names(indices)
akey             <- simpleKey(text=names(indsN01), points=F, lines=F, columns=3, cex=1.5, col=c("red","black","blue","gray","orange","magenta"))
#akey$text$lab[1] <- "BTS-ISIS"; akey$text$lab[2] <- "SNS"

tiff(filename = paste0(outPath,"Surveys_age.tif"),units = "cm", width = 20,height = 16,
     pointsize = 12,res = 150)
xyplot(data~year | factor(age), data=indsN01, type="b", key=akey, groups=qname, pch=19, 
       col=c("red","black","blue","gray","orange","magenta"),as.table=TRUE, scales="free", 
       layout=c(3,3), xlim=c(1995,(assYear-1)), ylim=c(-1.5,5))
dev.off()

for (i in c(1,2)) {
  plotInternalConsistency(indices[[i]], mark.last=T)
}


#plot full and survey indices and those that are cut from when ageing started
#SNS full years
# tiff(filename = paste0(outPath,"Consistency_SNS_old.tif"),units = "cm", width = 20,height = 20,
#      pointsize = 12,res = 150)
# plotInternalConsistency(indices[[1]], mark.last=T)
# dev.off()

#SNS from 2004
tiff(filename = paste0(outPath,"Consistency_SNS_2004.tif"),units = "cm", width = 20,height = 20,
     pointsize = 12,res = 150)
plotInternalConsistency(window(indices[[1]], start=2004), mark.last=T)
dev.off()

#BTS
# tiff(filename = paste0(outPath,"Consistency_BTS_old.tif"),units = "cm", width = 20,height = 20,
#      pointsize = 12,res = 150)
# plotInternalConsistency(indices[[2]], mark.last=T)
# dev.off()

#BTS from 1991
tiff(filename = paste0(outPath,"Consistency_BTS_1991.tif"),units = "cm", width = 20,height = 20,
     pointsize = 12,res = 150)
plotInternalConsistency(window(indices[[2]],start=1991), mark.last=T)
dev.off()

#plot unstructured indices
indices_unstruct  <- readFLIndices(paste(dataPath,"fleet.txt", sep=""), na.strings="-1")
maxN <- max(index(indices_unstruct[[5]]))
tiff(filename = paste0(outPath,"NL_LPUE_1995.tif"),units = "cm", width = 20,height = 20,
     pointsize = 12,res = 150)
plot(index(indices_unstruct[[5]]), ylim=c(0,maxN+0.01),type="o",ylab="index",lwd=2,pch=19,cex=1.5)
dev.off()

##Plot model weights
maxA  <- 10
#cols <- rich.colors(maxA)
cols <- brewer.pal(maxA,"Spectral")
stock.wt(stock)[stock.wt(stock)==0] <- NA

tiff(filename = paste0(outPath,"Stock_weights_1975_model.tif"),units = "cm", width = 20,height = 20,
     pointsize = 12,res = 150)
plot(x=1975:(assYear-1), y=stock.wt(stock)[1,], ylim=c(0,9.1), xlab="Year", cex.lab=1.2,ylab="Weights (kg)", main="Stock weight@age", lwd=2, col="white" )
for (aa in 1:maxA){
  points(x=1975:(assYear-1),y=as.numeric(stock.wt(stock)[aa,]), pch= if (aa<10) as.character(aa) else "+", col=cols[aa])
  #  lines(x=1975:(assYear-1),y=as.numeric(stock.wt(stock)[aa,]), pch= if (aa<10) as.character(aa) else "+", col=cols[aa])
}
dev.off()


landings.wt(stock)[landings.wt(stock)==0] <- NA

##----landing---
tiff(filename = paste0(outPath,"landing_weights_model.tif"),units = "cm", width = 20,height = 20,
     pointsize = 12,res = 150)
plot(x=1975:(assYear-1), y=landings.wt(stock)[1,], ylim=c(0,9.1), xlab="Year", cex.lab=1.2,ylab="Weights (kg)", main="Landing weight@age", lwd=2, col="white" )
for (aa in 1:maxA){
  points(x=1975:(assYear-1),y=as.numeric(landings.wt(stock)[aa,]), pch= if (aa<10) as.character(aa) else "+", col=cols[aa])
}
dev.off()


########################################################################################
# Discards
# discards.wt(stock)[discards.wt(stock) < 0.05] <- NA
# ##----discards---
# plot(x=1975:(assYear-1), y=discards.wt(stock)[1,], ylim=c(0,1.1), xlab="Year", cex.lab=1.2,ylab="Weights (kg)", main="Discards weight@age", lwd=2, col="white" )
# for (aa in 1:8){
#   points(x=1975:(assYear-1),y=as.numeric(discards.wt(stock)[aa,]), pch= if (aa<10) as.character(aa) else "+", col=cols[aa])
# }

# catch.wt(stock)[discards.wt(stock) < 0.05] <- NA
# ##----catch---
# plot(x=1975:(assYear-1), y=catch.wt(stock)[1,], ylim=c(0,9.1), xlab="Year", cex.lab=1.2,ylab="Weights (kg)", main="Catch weight@age", lwd=2, col="white" )
# for (aa in 1:10){
#   points(x=1975:(assYear-1),y=as.numeric(catch.wt(stock)[aa,]), pch= if (aa<10) as.character(aa) else "+", col=cols[aa])
# }

### ------------------------------------------------------------------------------------------------------
###   2. Read and process assessment input data
### ------------------------------------------------------------------------------------------------------

## Read stock data
stock               <- readFLStock(paste(dataPath,"index_raw.txt", sep=""))
units(stock)[1:17]  <- as.list(c(rep(Units,4), "NA", "NA", "f", "NA", "NA"))
# Change old ages landings to 0 (instead of NA) to allow plusgroup weight calculation
# Calculate plusgroup weight for stock
stkWtPgrp           <- stock.wt(stock)[maxA,]
# Apply max age
if (pGrp) stock     <- setPlusGroup(stock, plusgroup=maxA) else stock <- trim(stock, age=1:maxA)
# Trim if doing retro
stock.wt(stock)[maxA,] <- stkWtPgrp

# Number of years
years               <- as.numeric(range(stock)["minyear"]:range(stock)["maxyear"])
numYr               <- as.numeric(range(stock)["maxyear"]-range(stock)["minyear"]+1)
# Number of ages
numAges             <- length(1:maxA)
startyr             <- range(stock)[["minyear"]]## Read index data

# NOTE: all index values should run to (assYear-1) - because only #yrs read in, so start of index is calculated back from (assYear-1) and this.
indices             <- readFLIndices(paste(dataPath,"fleet.txt", sep=""), na.strings="-1")
indices             <- FLIndices(list(indices[[1]],indices[[2]],indices[[5]]))
indexVals           <- lapply(indices, index)
numIndices          <- length(indexVals)
indMPs              <- list()
for(ind in names(indices))
  indMPs[[ind]]     <- as.numeric((range(indices[[ind]])["startf"]+range(indices[[ind]])["endf"])/2)

disc <- read.table(paste0(dataPath,"diton.txt"))
disc <- disc[-1,]
colnames(disc) <- c("year","discards")
str(disc)
disc$discards <- as.numeric(as.character(disc$discards))
disc$Year <- as.numeric(as.character(disc$year))

# plot raw weights stock and landings

##Plot model weights
maxA  <- 10
#cols <- rich.colors(maxA)
cols <- brewer.pal(maxA,"Spectral")
stock.wt(stock)[stock.wt(stock)==0] <- NA

tiff(filename = paste0(outPath,"Stock_weights_1975_raw.tif"),units = "cm", width = 20,height = 20,
     pointsize = 12,res = 150)
plot(x=1975:(assYear-1), y=stock.wt(stock)[1,], ylim=c(0,9.1), xlab="Year", cex.lab=1.2,ylab="Weights (kg)", main="Stock weight@age", lwd=2, col="white" )
for (aa in 1:maxA){
  points(x=1975:(assYear-1),y=as.numeric(stock.wt(stock)[aa,]), pch= if (aa<10) as.character(aa) else "+", col=cols[aa])
  #  lines(x=1975:(assYear-1),y=as.numeric(stock.wt(stock)[aa,]), pch= if (aa<10) as.character(aa) else "+", col=cols[aa])
}
dev.off()


landings.wt(stock)[landings.wt(stock)==0] <- NA

##----landing---
tiff(filename = paste0(outPath,"landing_weights_raw.tif"),units = "cm", width = 20,height = 20,
     pointsize = 12,res = 150)
plot(x=1975:(assYear-1), y=landings.wt(stock)[1,], ylim=c(0,9.1), xlab="Year", cex.lab=1.2,ylab="Weights (kg)", main="Landing weight@age", lwd=2, col="white" )
for (aa in 1:maxA){
  points(x=1975:(assYear-1),y=as.numeric(landings.wt(stock)[aa,]), pch= if (aa<10) as.character(aa) else "+", col=cols[aa])
}
dev.off()



###############################################################
# Bubble plot survey ages 
###############################################################
library(dplyr)
library(reshape2)
library(ggplot2)
SurvPath <- paste0(Path,"02_Turbot_survey_CPUE/")

Surv <- read.csv(paste0(SurvPath,"Surveys_age_Nhour.csv"))
head(Surv)
colnames(Surv)[5] <- "Param"

names(Surv)[6:16] <- gsub(x = names(Surv)[6:16], pattern = "X_", replacement = "")  

mdata <- melt(Surv, id=c("survey","year","Param","spec","ship"))
head(mdata)
tail(mdata)

# plot SNS 
SNS <- mdata[mdata$survey=="SNS",]
radius <- as.numeric(sqrt(abs(SNS$value) / pi)) 
png(filename = paste0(outPath,"SNS_n_hour_age.tif"),units = "cm", width = 20,height = 20,
         pointsize = 12,res = 150)
ggplot(data=SNS, aes(x=year,y=variable))+
  geom_point(size=radius) +
  facet_grid(~survey) +
  scale_y_discrete(name="Age")+
  theme_bw()
dev.off()

# plot BTS-ISIS
BTS <- mdata[mdata$survey=="BTS",]
radius <- as.numeric(BTS$value) 

png(filename = paste0(outPath,"BTS_IsIS_n_hour_age.tif"),units = "cm", width = 20,height = 20,
    pointsize = 12,res = 150)
ggplot(data=BTS, aes(x=year,y=variable))+
  geom_point(size=radius*4) +
  facet_grid(~survey) +
  scale_y_discrete(name="Age")+
  theme_bw()
dev.off()

ggplot(data=mdata, aes(x=year,y=value,color=variable))+
  geom_line(size=1) +
  geom_point() +
  facet_wrap(~survey, scales = "free_y") +
  theme_bw()

# only ages 1-5
Age <- mdata[mdata$variable %in% c(1:5),]

png(filename = paste0(outPath,"Track cohorts surveys.tif"),units = "cm", width = 20,height = 10,
    pointsize = 12,res = 150)
ggplot(data=Age, aes(x=year,y=value,color=variable))+
  geom_line(size=1) +
  geom_point() +
  facet_wrap(~survey, scales = "free_y") +
  scale_y_continuous(name="numbers per hour")+
  scale_color_brewer(name="Age",palette = "RdYlBu")+
  theme_bw()
dev.off()
