save(TUR.sam,TUR,TUR.tun,TUR.ctrl,TUR.retro,file=file.path(outPath, paste0(run,"_",sens,"assessmentOut.RData")))
# source(file.path(outPath,"../","retroResidual.r"))

pdf(file.path(outPath,paste0(run,"_",sens,"assessmentOut.pdf")))
residual.diagnostics(TUR.sam)
print(cor.plot(TUR.sam))
obscv.plot(TUR.sam)

# figure - catchabilities at age from HERAS
catch <- catchabilities(TUR.sam)
print(xyplot(value+ubnd+lbnd ~ age | fleet,catch,
             scale=list(alternating=FALSE,y=list(relation="free")),as.table=TRUE,
             type="l",lwd=c(2,1,1),col=c("black","grey","grey"),
             subset=fleet %in% c("SNS","BTS-ISIS","NL_LPUE_age"),
             main="Survey catchability parameters",ylab="Catchability",xlab="Age"))

obsvar.plot(TUR.sam)

# figure - fishing age selectivity per year
sel.pat <- merge(f(TUR.sam),fbar(TUR.sam),
               by="year",suffixes=c(".f",".fbar"))
sel.pat$sel <- sel.pat$value.f/sel.pat$value.fbar
sel.pat$age <- as.numeric(as.character(sel.pat$age))
print(xyplot(sel ~ age|sprintf("%i's",floor((year+2)/5)*5),sel.pat,
             groups=year,type="l",as.table=TRUE,
             scale=list(alternating=FALSE),
             main="Selectivity of the Fishery by Pentad",xlab="Age",ylab="F/Fbar"))

sel.pat$FiveYrs <- sprintf("%i's",floor((sel.pat$year)/5)*5)
sel.pat$TenYrs  <- sprintf("%i's", floor((sel.pat$year+2)/10)*10)

# ggplot figure
print(ggplot(data= sel.pat, aes(x=age,y=sel,group=year)) +
      geom_line(color=sel.pat$year) +
      facet_wrap(~FiveYrs) +
      scale_x_continuous(limits = c(1,8),breaks=seq(0,8,2))+
      theme_bw()+
      labs(title= "Selectivity of the Fishery by Pentad",y="F/Fbar", x = "Age"))

################################
# outcomes of the assessment
###############################
print(plot(TUR.sam))

print(
ggplot(data=ssb(TUR.sam), aes(x=year,y=value)) +
  geom_line(size=1.2,color="black") +
  geom_ribbon(aes(ymin=lbnd, ymax=ubnd, x=year),fill = "grey50",alpha=0.3) +
  scale_x_continuous(limits=c(min(ssb(TUR.sam)$year),max(ssb(TUR.sam)$year)),
                     breaks=seq(min(ssb(TUR.sam)$year),max(ssb(TUR.sam)$year),5))+
  scale_y_continuous(name="SSB",limits=c(0,max(ssb(TUR.sam)$ubnd)*1.2)) +
  theme(strip.text        = element_text(face="bold",size=12),
        strip.background  = element_rect(fill="white",colour="black",size=0.3),
        panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=10),
        axis.text.y       = element_text(colour="black", size=10),
        axis.title.y      = element_text(angle=90, size=12),
        axis.title.x      = element_text(size=12),
        plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm")) +
  geom_hline(yintercept=6353,linetype="dashed", color = "black",size=1.1) +
  geom_hline(yintercept=4163,linetype="dotted", color = "black",size=1.1) +
  geom_hline(yintercept=2974,linetype="dashed", color = "red",size=1.1) +
  annotate(geom="text", x=max(ssb(TUR.sam)$year)-2, y=2500, label="Blim",color="red") +
  annotate(geom="text", x=max(ssb(TUR.sam)$year)-1, y=5800, label="MSYBtrigger",color="black") +
  annotate(geom="text", x=max(ssb(TUR.sam)$year)-2, y=3890, label="Bpa",color="black"))

print(
ggplot(data=fbar(TUR.sam), aes(x=year,y=value)) +
  geom_line(size=1.2,color="black") +
  geom_ribbon(aes(ymin=lbnd, ymax=ubnd, x=year),fill = "grey50",alpha=0.3) +
  scale_x_continuous(limits=c(min(fbar(TUR.sam)$year),max(fbar(TUR.sam)$year)),
                     breaks=seq(min(fbar(TUR.sam)$year),max(fbar(TUR.sam)$year),5))+
  scale_y_continuous(name="fbar(ages 2-6)",limits=c(0,max(fbar(TUR.sam)$ubnd)*1.1)) +
  theme(strip.text        = element_text(face="bold",size=12),
        strip.background  = element_rect(fill="white",colour="black",size=0.3),
        panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=10),
        axis.text.y       = element_text(colour="black", size=10),
        axis.title.y      = element_text(angle=90, size=12),
        axis.title.x      = element_text(size=12),
        plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm")) +
  geom_hline(yintercept=0.61,linetype="dashed", color = "black",size=1.1) +
  geom_hline(yintercept=0.36, color = "red",size=1.1) +
  geom_hline(yintercept=0.43, color = "black",linetype="dotted", size=1.1)+
  annotate(geom="text", x=min(fbar(TUR.sam)$year), y=0.58, label="Flim",color="black") +
  annotate(geom="text", x=min(fbar(TUR.sam)$year), y=0.41, label="Fpa",color="black") +
  annotate(geom="text", x=min(fbar(TUR.sam)$year), y=0.34, label="Fmsy",color="red"))

# long term geometric mean
TUR.srr <- exp(mean(log(rec(TUR.sam)$value)))
print(
ggplot(data=rec(TUR.sam), aes(x=year,y=value)) +
  geom_line(size=1.2,color="black") +
  geom_ribbon(aes(ymin=lbnd, ymax=ubnd, x=year),fill = "grey50",alpha=0.3) +
  scale_x_continuous(limits=c(min(rec(TUR.sam)$year),max(rec(TUR.sam)$year)),
                     breaks=seq(min(rec(TUR.sam)$year),max(rec(TUR.sam)$year),5))+
  scale_y_continuous(name="recruitment",limits=c(0,max(rec(TUR.sam)$ubnd)*1.1)) +
  theme(strip.text        = element_text(face="bold",size=12),
        strip.background  = element_rect(fill="white",colour="black",size=0.3),
        panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=10),
        axis.text.y       = element_text(colour="black", size=10),
        axis.title.y      = element_text(angle=90, size=12),
        axis.title.x      = element_text(size=12),
        plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm")) +
  geom_hline(yintercept= TUR.srr,linetype="dashed", color = "black",size=1.1))

###########################################################
# RESIDUALS
###########################################################

dat <- subset(residuals(TUR.sam),fleet=="catch unique")
print(xyplot(age ~ year,data=dat,cex=dat$std.res,col="black",main="Residuals by year Catch",
panel=function(...){
   lst <- list(...)
   panel.xyplot(lst$x,lst$y,pch=ifelse(lst$cex>0,1,19),col="black",cex=abs(lst$cex))
}))

dat1 <- dat[!dat$std.res == 0,]
print(
ggplot(dat1, aes(year, as.factor(age), size = abs(std.res))) +
      geom_point(shape = 21, aes(fill = factor(sign(std.res)))) +
      scale_size(name="scale",range = c(1, 10),breaks = seq(0,3,0.5)) +
      scale_fill_manual(name=" ", 
                        labels = c("-", "+"), 
                        values = c("black", "red")) +
      theme_bw() +
      labs(title= "Residuals by year in the Catch",y="Age", x = "Year"))

# residuals per year per age surveys
dat <- subset(residuals(TUR.sam),fleet %in% c("SNS","BTS-ISIS","NL_LPUE_age"))
print(xyplot(age ~ year|fleet,data=dat,cex=dat$std.res,col="black",main="Residuals by survey",
panel=function(...){
    lst <- list(...)
    panel.xyplot(lst$x,lst$y,pch=ifelse(lst$cex>0,1,19),col="black",cex=abs(lst$cex))
}))

dat3 <- dat[!dat$std.res == 0,]
print(
  ggplot(dat3, aes(year, as.factor(age), size = abs(std.res))) +
    facet_grid( ~ fleet) +
    geom_point(shape = 21, aes(fill = factor(sign(std.res)))) +
    scale_size(name="scale",range = c(1, 10),breaks = seq(0,3,0.5)) +
    scale_fill_manual(name=" ", 
                      labels = c("-", "+"), 
                      values = c("black", "red")) +
    scale_x_continuous(limits = c(min(dat3$year)-1,max(dat3$year)),
                       breaks=seq(min(dat3$year)-1,max(dat3$year),5)) +
   
    theme_bw()+
    labs(title= "Residuals by year in the surveys",y="Age", x = "Year"))

obscor.plot(TUR.sam)

print(plot(TUR.retro))
print(retroParams(TUR.retro))
print(retroSelectivity(TUR.retro,2007:2017))

# retrospectives for presentation purposes
# PLOT RETROs
SSB <- ssb(TUR.retro)
MHR <- round(mean(mohns.rho(TUR.retro,ref.year=max(SSB$year),span=5,type="ssb")$rho[1:5])/100,3)

print(
ggplot(data=SSB, aes(x=year,y=value,color=name)) +
  geom_line(size=1.1)+
  scale_y_continuous(name="ssb",limits=c(0,20000)) + 
  scale_x_continuous(breaks=seq(1981,max(SSB$year),5)) + 
  theme(legend.title = element_blank(),
        panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=10),
        axis.text.y       = element_text(colour="black", size=10),
        axis.title.y      = element_text(angle=90, size=12),
        axis.title.x      = element_text(size=12),
        plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm"))+
  labs(title = "5-year Retro for SSB")+
  annotate(geom="text",x=2010, y=17000, label=paste("Mohns rho ",MHR)))

FBAR <- fbar(TUR.retro)
MHR <- round(mean(mohns.rho(TUR.retro,ref.year=max(SSB$year),span=5,type="fbar")$rho[1:5])/100,3)

print(
ggplot(data=FBAR, aes(x=year,y=value,color=name)) +
  geom_line(size=1.1)+
  scale_y_continuous(name="Fbar",limits=c(0,1.2),breaks=seq(0,1.2,0.2)) + 
  scale_x_continuous(breaks=seq(1981,max(SSB$year),5)) + 
  theme(legend.title = element_blank(),
        panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=10),
        axis.text.y       = element_text(colour="black", size=10),
        axis.title.y      = element_text(angle=90, size=12),
        axis.title.x      = element_text(size=12),
        plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm"))+
  labs(title = "5-year Retro for Fbar") +
  annotate(geom="text",x=2010, y=1.1, label=paste("Mohns rho ",MHR)))

REC <- rec(TUR.retro)
MHR <- round(mean(mohns.rho(TUR.retro,ref.year=max(SSB$year),span=5,type="rec")$rho[1:5])/100,3)

print(
ggplot(data=REC, aes(x=year,y=value,color=name)) +
  geom_line(size=1.1)+
  scale_y_continuous(name="Recruitment",limits=c(0,10000),breaks=seq(0,10000,2000)) + 
  scale_x_continuous(breaks=seq(1981,max(SSB$year),5)) + 
  theme(legend.title = element_blank(),
        panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=10),
        axis.text.y       = element_text(colour="black", size=10),
        axis.title.y      = element_text(angle=90, size=12),
        axis.title.x      = element_text(size=12),
        plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm"))+
  labs(title = "5-year Retro for recruitment")+
  annotate(geom="text",x=2010, y=10000, label=paste("Mohns rho ",MHR)))


dev.off()

###############################
# Retro with confidence bounds#
###############################
dat <- cbind(rbindlist(setNames(lapply(c("ssb", "rec", "fbar"), function(x)
  do.call(x, list(TUR.retro))), c("SSB", "Rec", "F")), idcol="qname"), run="base")
colnames(dat)[3] <- "data"

dat <- as.data.frame(dat)
colnames(dat)[2] <- "year"
dat$run <- paste0(dat$run,"_",dat$year)

# 5 year mean mohns rho in percentages

monhsrho <- rbind(data.frame(qname="SSB", Value = round(mean(mohns.rho(TUR.retro,ref.year=assYear-1,span=5,type="ssb")$rho[1:5])/100, digits=4)),
                  data.frame(qname="F",Value=round(mean(mohns.rho(TUR.retro,ref.year=assYear-1,span=5,type="fbar")$rho[1:5])/100,digits=4)),
                  data.frame(qname="Rec",Value=round(mean(mohns.rho(TUR.retro,ref.year=assYear-1,span=5,type="rec")$rho[1:5])/100,digits=4)))
monhsrho$y <- c(14000, 0.80, 14000)

print(
ggplot(dat, aes(x=data,y=value,colour=run))+
  geom_line(aes(y=value),size=1.2) + ylim(c(0,NA)) +
  geom_ribbon(data=dat[dat$run == "base_2019", ],
              aes(ymin=lbnd, ymax=ubnd), alpha=0.3, colour="grey") +
  facet_grid(qname~., scales="free_y") +
  theme(legend.position="right")+
  theme_bw()+
  geom_text(data=monhsrho, aes(x=2016, y=y, label=Value, fill=NULL, colour=NULL),show.legend = FALSE)
)

head(dat)
SSB <- dat[dat$qname=="SSB",]

print(
ggplot(SSB, aes(x=data,y=value,colour=run))+
  geom_line(aes(y=value),size=1.2) + 
  scale_y_continuous(limits = c(0,20000),breaks = seq(0,20000,5000),expression("SSB (Tonnes)"), expand = c(0,0))+
  geom_ribbon(data=SSB[SSB$run == "base_2019", ],
              aes(ymin=lbnd, ymax=ubnd), alpha=0.3, colour="grey") +
  facet_grid(qname~., scales="free_y") +
  theme(legend.position="right")+
  theme_bw()+
  geom_text(data=monhsrho[monhsrho$qname=="SSB",], aes(x=2016, y=y, label=Value, fill=NULL, colour=NULL),show.legend = FALSE)
)

FBAR <- dat[dat$qname=="F",]
print(
ggplot(FBAR, aes(x=data,y=value,colour=run))+
  geom_line(aes(y=value),size=1.2) + 
  scale_y_continuous(limits = c(0,1.5),breaks = seq(0,1.5,0.25),expression("F"), expand = c(0,0))+
  geom_ribbon(data=FBAR[FBAR$run == "base_2019", ],
              aes(ymin=lbnd, ymax=ubnd), alpha=0.3, colour="grey") +
  facet_grid(qname~., scales="free_y") +
  theme(legend.position="right")+
  theme_bw()+
  geom_text(data=monhsrho[monhsrho$qname=="F",], aes(x=2016, y=y, label=Value, fill=NULL, colour=NULL),show.legend = FALSE)
)

REC <- dat[dat$qname=="Rec",]
print(
ggplot(REC, aes(x=data,y=value,colour=run))+
  geom_line(aes(y=value),size=1.2) + 
  scale_y_continuous(limits = c(0,15000),breaks = seq(0,15000,2000),expression("Recruitment (Thousands)"), expand = c(0,0))+
  geom_ribbon(data=REC[REC$run == "base_2019", ],
              aes(ymin=lbnd, ymax=ubnd), alpha=0.3, colour="grey") +
  facet_grid(qname~., scales="free_y") +
  theme(legend.position="right")+
  theme_bw()+
  geom_text(data=monhsrho[monhsrho$qname=="Rec",], aes(x=2016, y=y, label=Value, fill=NULL, colour=NULL),show.legend = FALSE)
)
