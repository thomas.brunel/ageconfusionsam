#save(TUR.sam,TUR,TUR.tun,TUR.ctrl,TUR.retro,file=paste0(outPath, paste0(run,"_",sens,"assessmentOut.RData")))

#pdf(file.path(outPath,paste0(run,"_",sens,"assessmentOut.pdf")))

#load("C:/Users/batsl001/OneDrive - WageningenUR/Bestandsbeheer/WGNSSK/2020/Stocks/TUR/00 rerun 2019 assessment/5_Assessment/Output/TUR.27.4_re-run_2019_sam_assessment.Rdata")

# source(file.path(outPath,"../","retroResidual.r"))
jpeg(file=paste0(figPath,"cor.plot.jpeg"),width=25,height=20,units="cm",res=150,pointsize=9)
print(cor.plot(TUR.sam))
dev.off()

png(file=paste0(figPath,"obscv.plot.png"),width=25,height=20,units="cm",res=150,pointsize=9)
obscv.plot(TUR.sam)
dev.off()

# figure - catchabilities at age 
catch <- catchabilities(TUR.sam)

png(file=paste0(figPath,"catchabilities_surveys.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(xyplot(value+ubnd+lbnd ~ age | fleet,catch,
             scale=list(alternating=FALSE,y=list(relation="free")),as.table=TRUE,
             type="l",lwd=c(2,1,1),col=c("black","grey","grey"),
             subset=fleet %in% c("SNS","BTS-ISIS","NL_LPUE_age"),
             main="Survey catchability parameters",ylab="Catchability",xlab="Age"))
dev.off()

png(file=paste0(figPath,"obsvar.plot.png"),width=25,height=20,units="cm",res=150,pointsize=9)
obsvar.plot(TUR.sam)
dev.off()

# figure - fishing age selectivity per year
sel.pat <- merge(f(TUR.sam),fbar(TUR.sam),
               by="year",suffixes=c(".f",".fbar"))
# selectivity = f-by-age/fbar(2-6)
sel.pat$sel <- sel.pat$value.f/sel.pat$value.fbar
sel.pat$age <- as.numeric(as.character(sel.pat$age))
sel.pat$FiveYrs <- sprintf("%i's",floor((sel.pat$year)/5)*5)
sel.pat$TenYrs  <- sprintf("%i's", floor((sel.pat$year+2)/10)*10)

# ggplot figure
png(file=paste0(figPath,"sel.pat.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(ggplot(data= sel.pat, aes(x=age,y=sel,group=year)) +
        geom_line(color=sel.pat$year) +
        facet_wrap(~FiveYrs) +
        scale_x_continuous(limits = c(1,8),breaks=seq(0,8,2))+
        theme_bw()+
        labs(title= "Selectivity of the Fishery by Pentad",y="F/Fbar", x = "Age"))
dev.off()

#########################
# plot TUR.sam 
#########################
png(file=paste0(figPath,"Assess_outcomes.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(plot(TUR.sam))
dev.off()

png(file=paste0(figPath,"SSB_limits.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(ggplot(data=ssb(TUR.sam), aes(x=year,y=value)) +
  geom_line(size=1.2,color="black") +
  geom_ribbon(aes(ymin=lbnd, ymax=ubnd, x=year),fill = "grey50",alpha=0.3) +
  scale_x_continuous(limits=c(min(ssb(TUR.sam)$year),max(ssb(TUR.sam)$year)),
                     breaks=seq(min(ssb(TUR.sam)$year),max(ssb(TUR.sam)$year),5))+
  scale_y_continuous(name="SSB",limits=c(0,max(ssb(TUR.sam)$ubnd)*1.2)) +
  theme(strip.text        = element_text(face="bold",size=12),
        strip.background  = element_rect(fill="white",colour="black",size=0.3),
        panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=10),
        axis.text.y       = element_text(colour="black", size=10),
        axis.title.y      = element_text(angle=90, size=12),
        axis.title.x      = element_text(size=12),
        plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm")) +
  geom_hline(yintercept=6353,linetype="dashed", color = "black",size=1.1) +
  geom_hline(yintercept=4163,linetype="dotted", color = "black",size=1.1) +
  geom_hline(yintercept=2974,linetype="dashed", color = "red",size=1.1) +
  annotate(geom="text", x=max(ssb(TUR.sam)$year)-2, y=2500, label="Blim",color="red") +
  annotate(geom="text", x=max(ssb(TUR.sam)$year)-1, y=5800, label="MSYBtrigger",color="black") +
  annotate(geom="text", x=max(ssb(TUR.sam)$year)-2, y=3890, label="Bpa",color="black"))
dev.off()

png(file=paste0(figPath,"fbar_limits.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(ggplot(data=fbar(TUR.sam), aes(x=year,y=value)) +
  geom_line(size=1.2,color="black") +
  geom_ribbon(aes(ymin=lbnd, ymax=ubnd, x=year),fill = "grey50",alpha=0.3) +
  scale_x_continuous(limits=c(min(fbar(TUR.sam)$year),max(fbar(TUR.sam)$year)),
                     breaks=seq(min(fbar(TUR.sam)$year),max(fbar(TUR.sam)$year),5))+
  scale_y_continuous(name="fbar(ages 2-6)",limits=c(0,max(fbar(TUR.sam)$ubnd)*1.1)) +
  theme(strip.text        = element_text(face="bold",size=12),
        strip.background  = element_rect(fill="white",colour="black",size=0.3),
        panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=10),
        axis.text.y       = element_text(colour="black", size=10),
        axis.title.y      = element_text(angle=90, size=12),
        axis.title.x      = element_text(size=12),
        plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm")) +
  geom_hline(yintercept=0.61,linetype="dashed", color = "black",size=1.1) +
  geom_hline(yintercept=0.36, color = "red",size=1.1) +
  geom_hline(yintercept=0.86, color = "black",linetype="dotted", size=1.1)+
  annotate(geom="text", x=min(fbar(TUR.sam)$year), y=0.58, label="Flim",color="black") +
  annotate(geom="text", x=min(fbar(TUR.sam)$year), y=0.81, label="Fp.05",color="black") +
  annotate(geom="text", x=min(fbar(TUR.sam)$year), y=0.34, label="Fmsy",color="red"))
dev.off()

TUR.srr <- exp(mean(log(rec(TUR.sam)$value)))

png(file=paste0(figPath,"Rec_limits.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(ggplot(data=rec(TUR.sam), aes(x=year,y=value)) +
  geom_line(size=1.2,color="black") +
  geom_ribbon(aes(ymin=lbnd, ymax=ubnd, x=year),fill = "grey50",alpha=0.3) +
  scale_x_continuous(limits=c(min(rec(TUR.sam)$year),max(rec(TUR.sam)$year)),
                     breaks=seq(min(rec(TUR.sam)$year),max(rec(TUR.sam)$year),5))+
  scale_y_continuous(name="recruitment",limits=c(0,max(rec(TUR.sam)$ubnd)*1.1)) +
  theme(strip.text        = element_text(face="bold",size=12),
        strip.background  = element_rect(fill="white",colour="black",size=0.3),
        panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=10),
        axis.text.y       = element_text(colour="black", size=10),
        axis.title.y      = element_text(angle=90, size=12),
        axis.title.x      = element_text(size=12),
        plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm")) +
  geom_hline(yintercept= TUR.srr,linetype="dashed", color = "black",size=1.1))
dev.off()

#############################
# Residuals
#############################

# if residual >0 then use white round (pch=1) else < 0 use black (pch=19)
dat <- subset(residuals(TUR.sam),fleet=="catch unique")
print(xyplot(age ~ year,data=dat,cex=dat$std.res,col="black",main="Residuals by year Catch",
panel=function(...){
   lst <- list(...)
   panel.xyplot(lst$x,lst$y,pch=ifelse(lst$cex>0,1,19),col="black",cex=abs(lst$cex))
}))

# residuals per year per age surveys
dat2 <- subset(residuals(TUR.sam),fleet %in% c("SNS","BTS-ISIS","NL_LPUE_age"))
print(xyplot(age ~ year|fleet,data=dat2,cex=dat$std.res,col="black",main="Residuals by survey",
panel=function(...){
    lst <- list(...)
    panel.xyplot(lst$x,lst$y,pch=ifelse(lst$cex>0,1,19),col="black",cex=abs(lst$cex))
}))

dev.off()

png(file=paste0(figPath,"obscor.plot.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(obscor.plot(TUR.sam))
dev.off()

png(file=paste0(figPath,"retro.param.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(retroParams(TUR.retro))
dev.off()

png(file=paste0(figPath,"retro.selectivity.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(retroSelectivity(TUR.retro,2007:assYear-1))
dev.off()


############################################################
# residual plot using ggplot but with legend
# if residual >0 then use white round (pch=1) else < 0 use black (pch=19)
dat1 <- dat[!dat$std.res == 0,]
png(file=paste0(figPath,"catch_residuals.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(ggplot(dat1, aes(year, as.factor(age), size = abs(std.res))) +
        geom_point(shape = 21, aes(fill = factor(sign(std.res)))) +
        scale_size(name="scale",range = c(1, 10),breaks = seq(0,3,0.5)) +
        scale_fill_manual(name=" ", 
                          labels = c("-", "+"), 
                          values = c("black", "red")) +
        ylab("age") +
        theme_bw())
dev.off()

# residual plot surveys but using ggplot
dat3 <- dat2[!dat2$std.res == 0,]
png(file=paste0(figPath,"survey_residuals.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(ggplot(dat3, aes(year, as.factor(age), size = abs(std.res))) +
        facet_grid( ~ fleet) +
        geom_point(shape = 21, aes(fill = factor(sign(std.res)))) +
        scale_size(name="scale",range = c(1, 10),breaks = seq(0,3,0.5)) +
        scale_fill_manual(name=" ", 
                          labels = c("-", "+"), 
                          values = c("black", "red")) +
        scale_x_continuous(limits = c(min(dat3$year)-1,max(dat3$year)),
                           breaks=seq(min(dat3$year)-1,max(dat3$year),5)) +
        ylab("age") +
        theme_bw())
dev.off()

# PLOT RETROs
SSB <- ssb(TUR.retro)
MHR <- round(mean(mohns.rho(TUR.retro,ref.year=max(SSB$year),span=5,type="ssb")$rho[1:5])/100,3)

png(file=paste0(figPath,"Retro_SSB.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(ggplot(data=SSB, aes(x=year,y=value,color=name)) +
    geom_line(size=1.1)+
    scale_y_continuous(name="ssb",limits=c(0,20000)) + 
    scale_x_continuous(breaks=seq(1981,max(SSB$year),5)) + 
    theme(legend.title = element_blank(),
          panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
          panel.grid.minor  = element_line(colour="white"),
          panel.background  = element_rect(fill="white"),
          panel.border      = element_rect(colour="black", fill=NA, size=0.5),
          axis.text.x       = element_text(colour="black", size=10),
          axis.text.y       = element_text(colour="black", size=10),
          axis.title.y      = element_text(angle=90, size=12),
          axis.title.x      = element_text(size=12),
          plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm"))+
    labs(title = "5-year Retro for SSB")+
    annotate(geom="text",x=2010, y=17000, label=paste("Mohns rho ",MHR)))
dev.off()

FBAR <- fbar(TUR.retro)
MHR <- round(mean(mohns.rho(TUR.retro,ref.year=max(SSB$year),span=5,type="fbar")$rho[1:5])/100,3)

png(file=paste0(figPath,"Retro_Fbar.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(ggplot(data=FBAR, aes(x=year,y=value,color=name)) +
    geom_line(size=1.1)+
    scale_y_continuous(name="Fbar",limits=c(0,1.2),breaks=seq(0,1.2,0.2)) + 
    scale_x_continuous(breaks=seq(1981,max(SSB$year),5)) + 
    theme(legend.title = element_blank(),
          panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
          panel.grid.minor  = element_line(colour="white"),
          panel.background  = element_rect(fill="white"),
          panel.border      = element_rect(colour="black", fill=NA, size=0.5),
          axis.text.x       = element_text(colour="black", size=10),
          axis.text.y       = element_text(colour="black", size=10),
          axis.title.y      = element_text(angle=90, size=12),
          axis.title.x      = element_text(size=12),
          plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm"))+
    labs(title = "5-year Retro for Fbar") +
    annotate(geom="text",x=2010, y=1.1, label=paste("Mohns rho ",MHR)))
dev.off()

REC <- rec(TUR.retro)
MHR <- round(mean(mohns.rho(TUR.retro,ref.year=max(SSB$year),span=5,type="rec")$rho[1:5])/100,3)

png(file=paste0(figPath,"Retro_Rec.png"),width=25,height=20,units="cm",res=150,pointsize=9)
print(ggplot(data=REC, aes(x=year,y=value,color=name)) +
    geom_line(size=1.1)+
    scale_y_continuous(name="Recruitment",limits=c(0,10000),breaks=seq(0,10000,2000)) + 
    scale_x_continuous(breaks=seq(1981,max(SSB$year),5)) + 
    theme(legend.title = element_blank(),
          panel.grid.major  = element_line(colour="grey85",linetype="dashed"),
          panel.grid.minor  = element_line(colour="white"),
          panel.background  = element_rect(fill="white"),
          panel.border      = element_rect(colour="black", fill=NA, size=0.5),
          axis.text.x       = element_text(colour="black", size=10),
          axis.text.y       = element_text(colour="black", size=10),
          axis.title.y      = element_text(angle=90, size=12),
          axis.title.x      = element_text(size=12),
          plot.margin       = unit(c(0.4,0.4,0.1,0.4), "cm"))+
    labs(title = "5-year Retro for recruitment")+
    annotate(geom="text",x=2010, y=10000, label=paste("Mohns rho ",MHR)))
dev.off()

######################################
# PLOT RETRO WITH CONFIDENCE BOUNDS
######################################
library(data.table)
dat <- cbind(rbindlist(setNames(lapply(c("ssb", "rec", "fbar"), function(x)
  do.call(x, list(TUR.retro))), c("SSB", "Rec", "F")), idcol="qname"), run="base")
colnames(dat)[3] <- "data"

dat <- as.data.frame(dat)
colnames(dat)[2] <- "year"
dat$run <- paste0(dat$run,"_",dat$year)

# 5 year mean mohns rho in percentages

monhsrho <- rbind(data.frame(qname="SSB", Value = round(mean(mohns.rho(TUR.retro,ref.year=assYear-1,span=5,type="ssb")$rho[1:5])/100, digits=4)),
                  data.frame(qname="F",Value=round(mean(mohns.rho(TUR.retro,ref.year=assYear-1,span=5,type="fbar")$rho[1:5])/100,digits=4)),
                  data.frame(qname="Rec",Value=round(mean(mohns.rho(TUR.retro,ref.year=assYear-1,span=5,type="rec")$rho[1:5])/100,digits=4)))
monhsrho$y <- c(14000, 0.80, 14000)

png(paste0(figPath,"Full_retro.png"),units = "cm", width = 30,height = 30,
    pointsize = 12,res = 150)
print(ggplot(dat, aes(x=data,y=value,colour=run))+
  geom_line(aes(y=value),size=1.2) + ylim(c(0,NA)) +
  geom_ribbon(data=dat[dat$run == "base_2019", ],
              aes(ymin=lbnd, ymax=ubnd), alpha=0.3, colour="grey") +
  facet_grid(qname~., scales="free_y") +
  theme(legend.position="right")+
  theme_bw()+
  geom_text(data=monhsrho, aes(x=2016, y=y, label=Value, fill=NULL, colour=NULL),show.legend = FALSE))
dev.off()

head(dat)
SSB <- dat[dat$qname=="SSB",]
png(paste0(figPath,"SSB_retro_confidence_bound.png"),units = "cm", width = 30,height = 20,
    pointsize = 12,res = 150)
print(ggplot(SSB, aes(x=data,y=value,colour=run))+
  geom_line(aes(y=value),size=1.2) + 
  scale_y_continuous(limits = c(0,20000),breaks = seq(0,20000,5000),expression("SSB (Tonnes)"), expand = c(0,0))+
  geom_ribbon(data=SSB[SSB$run == "base_2019", ],
              aes(ymin=lbnd, ymax=ubnd), alpha=0.3, colour="grey") +
  facet_grid(qname~., scales="free_y") +
  theme(legend.position="right")+
  theme_bw()+
  geom_text(data=monhsrho[monhsrho$qname=="SSB",], aes(x=2016, y=y, label=Value, fill=NULL, colour=NULL),show.legend = FALSE))
dev.off()

FBAR <- dat[dat$qname=="F",]
png(paste0(figPath,"F_retro_confidence_bound.png"),units = "cm", width = 30,height = 20,
    pointsize = 12,res = 150)
print(ggplot(FBAR, aes(x=data,y=value,colour=run))+
  geom_line(aes(y=value),size=1.2) + 
  scale_y_continuous(limits = c(0,1.5),breaks = seq(0,1.5,0.25),expression("F"), expand = c(0,0))+
  geom_ribbon(data=FBAR[FBAR$run == "base_2019", ],
              aes(ymin=lbnd, ymax=ubnd), alpha=0.3, colour="grey") +
  facet_grid(qname~., scales="free_y") +
  theme(legend.position="right")+
  theme_bw()+
  geom_text(data=monhsrho[monhsrho$qname=="F",], aes(x=2016, y=y, label=Value, fill=NULL, colour=NULL),show.legend = FALSE))
dev.off()

REC <- dat[dat$qname=="Rec",]
png(paste0(figPath,"Rec_retro_confidence_bound.png"),units = "cm", width = 30,height = 20,
    pointsize = 12,res = 150)
print(ggplot(REC, aes(x=data,y=value,colour=run))+
  geom_line(aes(y=value),size=1.2) + 
  scale_y_continuous(limits = c(0,15000),breaks = seq(0,15000,2000),expression("Recruitment (Thousands)"), expand = c(0,0))+
  geom_ribbon(data=REC[REC$run == "base_2019", ],
              aes(ymin=lbnd, ymax=ubnd), alpha=0.3, colour="grey") +
  facet_grid(qname~., scales="free_y") +
  theme(legend.position="right")+
  theme_bw()+
  geom_text(data=monhsrho[monhsrho$qname=="Rec",], aes(x=2016, y=y, label=Value, fill=NULL, colour=NULL),show.legend = FALSE))
dev.off()

# Plot F-at-age
colset      <- brewer.pal(9, "Set1")
F_at_age <- melt(TUR.sam@harvest, id=c("year"))
colnames(F_at_age)[7] <- "F"

png(file=paste0(figPath,"F-at-age.png"),width=25,height=20,units="cm",res=150,pointsize=9)

print(ggplot(F_at_age, aes(x=year,y=F,color=as.character(age)))+
  geom_line()+
  geom_point() +
  scale_color_brewer(palette = "Set1") +
  scale_y_continuous(limits = c(0,1.4), breaks=seq(0,1.4,0.2), expand = c(0,0)) +
  scale_x_continuous(limits=c(1980,assYear),breaks=seq(1980,assYear,5))+
  theme(strip.text        = element_text(face="bold",size=12),
        strip.background  = element_rect(fill="white",colour="black",size=0.3),
        panel.grid.major  = element_line(colour="white"),
        panel.grid.minor  = element_line(colour="white"),
        panel.background  = element_rect(fill="white"),
        panel.border      = element_rect(colour="black", fill=NA, size=0.5),
        axis.text.x       = element_text(colour="black", size=12, angle=0,vjust=0.5),
        axis.text.y       = element_text(colour="black", size=12),
        axis.title.y      = element_text(angle=90, size=14),
        axis.title.x      = element_text(size=14),
        legend.position   = "top",
        legend.text       = element_text(colour="black", size=12 ),
        legend.title      = element_blank(),
        legend.key        = element_rect(fill = "white"),
        legend.key.size   = unit(1, "cm"),
        panel.spacing.x   = unit(1.5, "lines"),
        panel.spacing.y   = unit(0.9, "lines"),
        plot.margin       = unit(c(0.4,1.1,0.1,1.1), "cm")))
dev.off()

###########################
# Catch cohorts
##########################
library(ggplotFL)
## catch cohort plot
myyear <- as.numeric(dimnames(TUR)$year)
f1 <- ggplot(TUR@catch.n, aes(x=cohort, y=data, group=age, color=age)) +
  geom_line() +
  #scale_color_distiller(palette="Spectral") +
  geom_point(colour='white', size=5) +
  geom_text(aes(label=age)) +
  ggtitle("Catch by cohort")+
  theme_bw() +
  xlab("Cohort") + ylab("") +
  scale_x_continuous(name="", breaks=myyear, labels=myyear, limits=c(min(myyear), max(myyear))) +
  theme(plot.title = element_text(color="black", size=20, face="bold"),
        legend.position = "right", legend.title = element_text(colour="black", size=15, face="bold"),
        legend.text = element_text(colour="black", size=10, face="plain"),
        axis.text.x = element_text(color = "black", size = 14, angle = 90),
        axis.text.y = element_text(color = "black", size = 14, angle = 0))

png(filename = paste0(figPath,"Catch_by_cohort catches.png"),units = "cm", width = 24,height = 16,
    pointsize = 12,res = 150)
print(f1)
dev.off()

# SNS
myyear <- as.numeric(dimnames(indices[[1]])$year)
f2 <- ggplot(indices[[1]]@catch.n, aes(x=cohort, y=data, group=age, color=age)) +
  geom_line() +
  #scale_color_distiller(palette="Spectral") +
  geom_point(colour='white', size=5) +
  geom_text(aes(label=age)) +
  ggtitle("Catch by cohort SNS")+
  theme_bw() +
  xlab("Cohort") + ylab("") +
  scale_x_continuous(name="", breaks=myyear, labels=myyear, limits=c(min(myyear), max(myyear))) +
  theme(plot.title = element_text(color="black", size=20, face="bold"),
        legend.position = "right", legend.title = element_text(colour="black", size=15, face="bold"),
        legend.text = element_text(colour="black", size=10, face="plain"),
        axis.text.x = element_text(color = "black", size = 14, angle = 90),
        axis.text.y = element_text(color = "black", size = 14, angle = 0))

png(filename = paste0(figPath,"Catch_by_cohort SNS.png"),units = "cm", width = 24,height = 16,
    pointsize = 12,res = 150)
print(f2)
dev.off()

# BTS-ISIS
myyear <- as.numeric(dimnames(indices[[2]])$year)
f3 <- ggplot(indices[[2]]@catch.n, aes(x=cohort, y=data, group=age, color=age)) +
  geom_line() +
  #scale_color_distiller(palette="Spectral") +
  geom_point(colour='white', size=5) +
  geom_text(aes(label=age)) +
  ggtitle("Catch by cohort BTS-ISIS")+
  theme_bw() +
  xlab("Cohort") + ylab("") +
  scale_x_continuous(name="", breaks=myyear, labels=myyear, limits=c(min(myyear), max(myyear))) +
  theme(plot.title = element_text(color="black", size=20, face="bold"),
        legend.position = "right", legend.title = element_text(colour="black", size=15, face="bold"),
        legend.text = element_text(colour="black", size=10, face="plain"),
        axis.text.x = element_text(color = "black", size = 14, angle = 90),
        axis.text.y = element_text(color = "black", size = 14, angle = 0))

png(filename = paste0(figPath,"Catch_by_cohort BTS.png"),units = "cm", width = 24,height = 16,
    pointsize = 12,res = 150)
print(f3)
dev.off()
