library(tidyr)
library(ggplot2)
library(stockassessment)
library(FLCore)
library(FLSAM)
source("./utils/FlrtoSam.r")  # load function FLSAM2SAM


#### load the otolith readings and reformat
agesamp <- read.csv("expertwhole.csv")
agesamp$R02.NL  <- as.numeric(agesamp$R02.NL)
agesamp$R04.BE  <- as.numeric(agesamp$R04.BE)
agesamp$R06.BE  <- as.numeric(agesamp$R06.BE)
agesamp <- gather(agesamp , key = reader , value = age , 4:6)
# plot the variations in reading
png("./plots/otolith exchange expert whole.png")
print(ggplot(agesamp  , aes(age,fill=Modal.age)) + geom_histogram(binwidth=1) + facet_wrap(~Modal.age))
dev.off()


# format for stockassessment
#remove fish with modal age outside of the age range of the assessment
agesamp <- subset(agesamp ,Modal.age <=8 & Modal.age > 0)
agesamp <- agesamp[,c("Image.ID","age")]
agesamp$data <- 1
names(agesamp)[1] <- "sample"
agesamp <- agesamp[,c("age","sample","data")]
agesamp <-agesamp[!is.na(agesamp$age),]


# load the data input data
load("./WGNSSK2021/TUR_27.4_Final_WGNSSK_2021_input_data.Rdata")
load("./WGNSSK2021/Final__WGNSSK_2021assessmentOut.RData")
TUR2 <-TUR + TUR.sam
load('tur.sam.RData')
sam.dat <-  FLSAM2SAM(FLStocks(TUR2),TUR.tun,agesampl = agesamp)     # NOTE : input contain age readings

# load turbot conf, and modify to include age reading error estimation
source('./utils/TURconf.r')
conf$keyAgeSampleData[1] <- 1
par<-defpar(sam.dat,conf)

# model fitting
fit2 <-sam.fit(sam.dat,conf,par,newtonsteps = 0)
           # NOTE there is now init par values for par$logAgeASD
sam.dat.save  <- sam.dat





# try again, with the matrix provided 
load("ACM Tur.RData")
# compute the matrix

sam.dat  <- sam.dat.save
p <- 0.4 # % ACM vs ID      55% i the max that worked
sam.dat$ageConfusion[[1]] <- p * ACM.cn + (1-p) * diag(nrow = 8)
sam.dat$agesampledata  <- cbind(age=numeric(0), sample=numeric(0), data=numeric(0))


source('./utils/TURconf.r')
par<-defpar(sam.dat,conf)





# model fitting
fit3 <-sam.fit(sam.dat,conf,par,newtonsteps = 0)          ### runs, but fails.....

load("tur.sam.RData")
fit ; fit3
plot(c(fit,fit3))

p1<- partable(fit)
p2<- partable(fit3)


pars <-  cbind(names(p1),p1[,3:2],p2[,3:2])
