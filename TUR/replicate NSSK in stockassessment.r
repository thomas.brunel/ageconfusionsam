
library(FLCore)
library(FLSAM)
source("./utils/FlrtoSam.r")  # load function FLSAM2SAM

# load input and output from WGNNSK
load("./WGNSSK2021/TUR_27.4_Final_WGNSSK_2021_input_data.Rdata")
load("./WGNSSK2021/Final__WGNSSK_2021assessmentOut.RData")
TUR2 <-TUR + TUR.sam

# convert FLSAM data input stockassessment inputs 
sam.dat <-  FLSAM2SAM(FLStocks(TUR2),TUR.tun,agesampl =  cbind(age=numeric(0), sample=numeric(0), data=numeric(0)))

# configuration as in WGNSSK
source('./utils/TURconf.r')
par<-defpar(sam.dat,conf)

# model fitting
fit <-sam.fit(sam.dat,conf,par)

# compare with FLSAM output
TUR.sam@nlogl
fit               # yes !!!!  same likelihood = same fit

ssbtable(fit)[,"Estimate"]
ssb(TUR2)

save(sam.dat,conf,par,fit,file='tur.sam.RData')





##### look for alternative configuration that would be more stable
source('./utils/TURconf2.r')
par<-defpar(sam.dat,conf)

# model fitting
fitbis <-sam.fit(sam.dat,conf,par)
fit ; fitbis
plot(c(fit,fitbis))

