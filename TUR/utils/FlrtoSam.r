ctrl2conf <- function(ctrl,data){
  conf              <- defcon(data)
  conf$minAge[]       <- ctrl@range["min"];
  conf$maxAge[]       <- ctrl@range["max"];
  conf$maxAgePlusGroup[] <- ifelse(ctrl@plus.group,1,0);
  conf$keyLogFsta[]   <- ctrl@states
  conf$corFlag[]      <- ctrl@cor.F
  conf$keyLogFpar[]   <- ctrl@catchabilities
  conf$keyQpow[]      <- ctrl@power.law.exps
  conf$keyVarF[]      <- ctrl@f.vars
  conf$keyVarLogN[]   <- ctrl@logN.vars;
  conf$keyVarLogP[]   <- if(is.na(ctrl@logP.vars[1])){numeric(0)}else{ctrl@logP.vars}
  conf$keyVarObs[]    <- ctrl@obs.vars
  conf$obsCorStruct[] <- factor(ctrl@cor.obs.Flag,levels=c("ID","AR","US"))
  conf$keyCorObs[]    <- ctrl@cor.obs
  conf$stockRecruitmentModelCode[] <- ctrl@srr
  conf$noScaledYears[] <- ctrl@scaleNoYears
  if(conf$noScaledYears>0){
    conf$keyScaledYears[]  <- ctrl@scaleYears
    conf$keyParScaledYA[] <- ctrl@scalePars
  } else {
    conf$keyScaledYears <- numeric()
    conf$keyParScaledYA <- matrix(numeric(),nrow=0,ncol=0)
  }
  conf$fbarRange[]    <- c(ctrl@range[c("minfbar","maxfbar")])
  conf$keyBiomassTreat[] <- rep(-1,data$noFleets)
  conf$keyBiomassTreat[which(data$fleetTypes %in% c(3,4))] <- ctrl@biomassTreat[which(ctrl@fleets %in% c(3,4))]
  conf$obsLikelihoodFlag[] <- factor(ctrl@likFlag,levels=c("LN","ALN"))
  conf$fixVarToWeight[] <- 0
return(conf)}

conf2ctrl <- function(conf,data){
  ctrl    <- new("FLSAM.control")
  ctrl@range <- c(min=conf$minAge,max=conf$maxAge,plusgroup=ifelse(conf$maxAgePlusGroup==1,conf$maxAge,NA),minyear=min(data$years),maxyear=max(data$years),minfbar=conf$fbarRange[1],maxfbar=conf$fbarRange[2])
  ctrl@fleets <- data$fleetTypes
  names(ctrl@fleets)  <- attr(data,"fleetNames")
    matdef            <- matrix(NA,nrow=length(ctrl@fleets),ncol=length(ctrl@range["min"]:ctrl@range["max"]),dimnames=list(names(ctrl@fleets),ctrl@range["min"]:ctrl@range["max"]))
    matdef[]          <- conf$keyLogFsta
  ctrl@states         <- matdef
  ctrl@cor.F          <- conf$corFlag
    matdef[]          <- conf$keyLogFpar
  ctrl@catchabilities <- matdef
    matdef[]          <- conf$keyQpow
  ctrl@power.law.exps <- matdef
    matdef[]          <- conf$keyVarF
  ctrl@f.vars         <- matdef
    matdef[]          <- conf$keyVarLogN
  ctrl@logN.vars      <- matdef
    matdef[]          <- conf$keyVarObs
  ctrl@obs.vars       <- matdef
  ctrl@cor.obs.Flag   <- conf$obsCorStruct
    ages              <- ctrl@range["min"]:ctrl@range["max"]
    matdef            <- matrix(NA,nrow=length(ctrl@fleets),ncol=length(ctrl@range["min"]:ctrl@range["max"])-1,dimnames=list(names(ctrl@fleets),apply(cbind(ages[-length(ages)],ages[-1]),1,paste,collapse="-")))
    matdef[]          <- conf$keyCorObs
  ctrl@cor.obs        <- matdef
  ctrl@srr            <- as.integer(conf$stockRecruitmentModelCode)
  ctrl@biomassTreat   <- conf$keyBiomassTreat
  ctrl@likFlag        <- conf$obsLikelihoodFlag
  ctrl@fixVarToWeight <- as.logical(conf$fixVarToWeight)
return(ctrl)}


FLSAM2SAM <- function(stcks,tun,sumFleets=NULL,catch.vars=NULL, agesampl){

          # stcks <- FLStocks(TUR2)          ;  tun <- TUR.tun     ; catch.vars=NULL


  allMax <- max(sapply(stcks,function(x) max(x@range["maxyear"])),
            max(sapply(tun,function(x) max(x@range[c("maxyear")]))))
  stcksMax <- max(sapply(stcks,function(x) max(x@range["maxyear"])))
  oldTunNames <- names(tun)
  newTunNames <-lapply(as.list(names(tun)),function(x){gsub(" ","",x)})
  names(tun) <- newTunNames

  if (stcksMax<allMax)  {
    stck <- stcks[[which.max(sapply(stcks,function(x) max(x@range["maxyear"])))]]
    stck<- window(stck,end=allMax,
              frequency=1,extend=TRUE)  # extend by one year
    stck@mat[,as.character((stcksMax+1):allMax),,,,]         <- stck@mat[,as.character(stcksMax),,,,]      # MV (there must be an easier way!!
    stck@stock.wt[,as.character((stcksMax+1):allMax),,,,]    <- stck@stock.wt[,as.character(stcksMax),,,,]
    stck@catch.wt[,as.character((stcksMax+1):allMax),,,,]    <- stck@catch.wt[,as.character(stcksMax),,,,]
    stck@discards.wt[,as.character((stcksMax+1):allMax),,,,] <- stck@discards.wt[,as.character(stcksMax),,,,]
    stck@landings.wt[,as.character((stcksMax+1):allMax),,,,] <- stck@landings.wt[,as.character(stcksMax),,,,]
    stck@m[,as.character((stcksMax+1):allMax),,,,]           <- stck@m[,as.character(stcksMax),,,,]
    stck@landings.n[,as.character((stcksMax+1):allMax),,,,]  <- stck@landings.n[,as.character(stcksMax),,,,]
    stck@harvest.spwn[,as.character((stcksMax+1):allMax),,,,]<- stck@harvest.spwn[,as.character(stcksMax),,,,]
    stck@m.spwn[,as.character((stcksMax+1):allMax),,,,]      <- stck@m.spwn[,as.character(stcksMax),,,,]
    stcks[[which.max(sapply(stcks,function(x) max(x@range["maxyear"])))]] <- stck
  }

  #- Prepare survey data
  surveysFLR  <- lapply(tun,function(x){ret <- t(x@index[,drop=T]);if(dim(ret)[1]==1) ret <- t(ret); return(ret)})
  dmnsFLR     <- lapply(tun,dims)
  typeFLR     <- lapply(tun,function(x){return(x@type)})
  for(iSurv in names(surveysFLR)){
    if(is.na(dmnsFLR[[iSurv]]$min) & dimnames(tun[[iSurv]]@index)$age[1]=="all"){
      colnames(surveysFLR[[iSurv]]) <- -1
    } else {
      colnames(surveysFLR[[iSurv]]) <- seq(dmnsFLR[[iSurv]]$min,dmnsFLR[[iSurv]]$max,1)
    }
    attr(surveysFLR[[iSurv]],"time") <- c(dmnsFLR[[iSurv]]$startf,dmnsFLR[[iSurv]]$endf)
    if(typeFLR[[iSurv]]=="partial")
      attr(surveysFLR[[iSurv]],"part") <- which(names(which(unlist(typeFLR)=="partial"))==iSurv)
  }

  #- Prepare catch data

    residual.fleets <- t(stcks[[1]]@catch.n[,,,,1,drop=T])


  #- Prepare props, natmort and stock weigth
    propMat     <- t(stcks[[1]]@mat[,,,,1,drop=T])
    propF       <- t(stcks[[1]]@harvest.spwn[,,,,1,drop=T])
    propM       <- t(stcks[[1]]@m.spwn[,,,,1,drop=T])
    stockWeight <- t(stcks[[1]]@stock.wt[,,,,1,drop=T])
    natMort     <- t(stcks[[1]]@m[,,,,1,drop=T])
    catchWeight <- t(stcks[[1]]@catch.wt[,,,,1,drop=T])
    discardWeight <- t(stcks[[1]]@discards.wt[,,,,1,drop=T])
    landingWeight <- t(stcks[[1]]@landings.wt[,,,,1,drop=T])
    landFrac   <- t(stcks[[1]]@landings.n[,,,,1,drop=T])/t(stcks[[1]]@catch.n[,,,,1,drop=T])
    landFrac[]   <-  1
     
library(stockassessment)
     
    sam.dat <-setup.sam.data(surveys=surveysFLR,
                              residual.fleet=residual.fleets, # Notice list
                              prop.mature=propMat,
                              stock.mean.weight=stockWeight,
                              catch.mean.weight=catchWeight,
                              dis.mean.weight=discardWeight,
                              land.mean.weight=landingWeight,
                              prop.f=propF,
                              prop.m=propM,
                              natural.mortality=natMort,
                              land.frac=landFrac,
                              agesampledata = agesampl )

  # Get data weighting
  if(is.null(catch.vars)==F){
    if("sum" %in% names(stcks)){
      idxSum      <- which(sam.dat$aux[,"fleet"] == which(sam.dat$fleetTypes == 7))
      sam.dat$weight[idxSum] <- c(t(catch.vars[["sum"]][,drop=T]))
    }
    counter     <- 1
    for(iFleet in which(sam.dat$fleetTypes==0)){
      idxRes    <- which(sam.dat$aux[,"fleet"] == iFleet)
      sam.dat$weight[idxRes] <- c(t(catch.vars[["residual"]][,,,,counter,drop=T]))
      counter   <- counter + 1
    }
  }

  tun.var       <- lapply(tun,index.var)
  for(iFleet in which(!sam.dat$fleetTypes %in% c(0,7))){
    iTun        <- which(names(tun)==attr(sam.dat,"fleetNames")[iFleet])
    idxTun      <- which(sam.dat$aux[,"fleet"] == iFleet)
    tunWeight   <- as.data.frame(tun.var[[iTun]])$data
    if(sam.dat$fleetTypes[iFleet]==6)
      tunWeight <- tunWeight[which(as.data.frame(tun[[iTun]]@index)$data>0)]
    sam.dat$weight[idxTun] <- tunWeight
  }
return(sam.dat)}

