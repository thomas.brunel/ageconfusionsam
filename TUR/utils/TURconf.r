
conf<-defcon(sam.dat)
#TUR.ctrl@catchabilities["SNS",ac(1:6)]      <- c(0,0,1,2,2,2)           + 101
conf$keyLogFpar[2,1:6] <-  c(0,0,1,2,2,2)
#TUR.ctrl@catchabilities["BTS-ISIS",ac(1:7)] <- c(0,0,1,1,2,2,2)         + 201
conf$keyLogFpar[3,1:7] <-  c(0,0,1,1,2,2,2) + 3
#TUR.ctrl@catchabilities["NL_LPUE",ac(1)]    <- 0                        + 301
conf$keyLogFpar[4,1] <-  6
#TUR.ctrl@f.vars["catch unique",]            <- c(0,1,2,2,3,3,4,4)
conf$keyVarF[1,] <-  c(0,1,2,2,3,3,4,4)
#TUR.ctrl@obs.vars["catch unique",]          <- c(0,1,2,2,3,3,4,4)       + 101
conf$keyVarObs[1,]  <- c(0,1,2,2,3,3,4,4)
#TUR.ctrl@obs.vars["SNS",ac(1:6)]            <- c(0,0,1,2,2,2)           + 201
conf$keyVarObs[2,1:6]  <-    c(0,0,1,2,2,2) + 5
#TUR.ctrl@obs.vars["BTS-ISIS",ac(1:7)]       <- c(0,0,0,1,2,2,2)         + 301
conf$keyVarObs[3,1:7]  <-    c(0,0,0,1,2,2,2) + 8
#TUR.ctrl@obs.vars["NL_LPUE",ac(1)]          <- 0                        + 401
conf$keyVarObs[4,1]  <-   11
##TUR.ctrl@cor.obs["BTS-ISIS",1:6]           <- c(0,1,1,1,1,1)
#TUR.ctrl@cor.obs["SNS",1:5]                 <- c(0,0,0,0,0)
conf$keyCorObs[2,1:5]  <-0
#TUR.ctrl@cor.obs.Flag[2]                    <- af("AR")
conf$obsCorStruct[2]  <- "AR"
#TUR.ctrl@biomassTreat[4]                    <- 2
conf$keyBiomassTreat[4]  <-2
##TUR.ctrl@residuals                         <- FALSE
#TUR.ctrl                                    <- update(TUR.ctrl)

