
conf<-defcon(sam.dat)

conf$keyLogFpar[2,1:6] <-  c(0,0,1,2,2,2)
conf$keyLogFpar[3,1:7] <-  c(0,0,1,1,2,2,2) + 3
conf$keyLogFpar[4,1] <-  6

conf$keyVarF[1,] <-  c(0,1,2,2,2,2,2,2)

conf$keyVarObs[1,]  <- c(0,1,2,2,3,3,4,4)
conf$keyVarObs[2,1:6]  <-    c(0,0,1,2,2,2) + 5
conf$keyVarObs[3,1:7]  <-    c(0,0,0,1,2,2,2) + 8
conf$keyVarObs[4,1]  <-   11

#conf$keyCorObs[2,1:5]  <-0

#conf$obsCorStruct[2]  <- "AR"

conf$keyBiomassTreat[4]  <-2

